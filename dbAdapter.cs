﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JuFanAPI
{
    public class dbAdapter
    {
        public static string strWhereParaKey = "@where_";
        public static string strWhereParaKeyNot = "@wherenot_";

        public static Object objTLock = new Object();
        public static Object objYLock = new Object();
        public static Object objSLock = new Object();

        public dbAdapter()
        {
            //
            // TODO: 在此加入建構函式的程式碼
            //
        }

        #region Get Data

        /// <summary>
        /// 取得DBConnection
        /// </summary>
        /// <returns></returns>
        public static SqlConnection getConnection()
        {
            SqlConnection conn = null;
            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["dbSqlJunfu_ConnStr"].ToString();
                conn = new SqlConnection(strConn);
                return conn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 依SQL查詢資料
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static DataTable getDataTable(SqlCommand cmd)
        {
            SqlConnection conn = null;
            DataTable dt = null;
            DataTable dtTmp = null;

            SqlDataAdapter adapter = null;
            int intTmp = 0;

            try
            {
                conn = getConnection();
                dt = new DataTable();
                cmd.Connection = conn;
                adapter = new SqlDataAdapter(cmd);

                intTmp = adapter.Fill(dt);

                dtTmp = new DataTable();
                if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
                {
                    dtTmp = dt.Clone();
                    for (int i = 0; i < dt.Rows.Count; i++)
                        dtTmp.ImportRow(dt.Rows[i]);
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return dtTmp;
        }

        /// <summary>
        /// 執行常數回傳值的SQL指令
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static Object getScalarBySQL(SqlCommand cmd)
        {
            SqlConnection conn = null;
            Object val = null;
            try
            {
                conn = getConnection();
                conn.Open();
                cmd.Connection = conn;

                val = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                conn.Close();
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return val;
        }

        public static DataTable SQLFunc(string sql)
        {
            SqlConnection conn = getConnection();
            // new SqlConnection(ConfigurationManager.ConnectionStrings["59ConnectionString"].ConnectionString);
            conn.Open();
            //取得資料
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }

        //資料庫連結-取得總數量
        public static int SQLFuncNum(string sql)
        {
            int temp_qty = 0;
            SqlConnection conn = getConnection();
            //new SqlConnection(ConfigurationManager.ConnectionStrings["59ConnectionString"].ConnectionString);

            try
            {
                conn.Open();
                //取得總數量     
                SqlCommand cmd = new SqlCommand(sql, conn);

                if (cmd.ExecuteScalar() != null)
                {
                    temp_qty = Byte.Parse(cmd.ExecuteScalar().ToString());
                }
                else
                {
                    temp_qty = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            // Byte.Parse(cmdTaskFunction.ExecuteScalar().ToString());
            conn.Close();
            return temp_qty;
        }


        //最新產生的一筆主索引

        public static string finalindex(string table, string index)
        {
            //取得資料
            string re_txt = "";
            DataTable dt = new DataTable();
            string sql = "SELECT top 1 " + index + " FROM [" + table + "] order by " + index + " desc";
            dt = SQLFunc(sql);

            if (dt.Rows.Count != 0)
            {

                re_txt = dt.Rows[0].ItemArray[0].ToString();

            }

            dt.Clear();
            return re_txt;
        }


        //單筆資料欄位
        public static string tablesubject(string table, string index, string inputid, string row)
        {
            //取得資料
            string re_txt = "";
            //DataTable dt = new DataTable();
            //string sql = "SELECT " + row + " FROM [" + table + "] WHERE ([" + index + "] = '" + inputid + "') ";
            //dt = SQLFunc(sql);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@inputid", inputid);
            cmd.CommandText = string.Format("SELECT {0} FROM [{1}] WHERE ([{2}] = @inputid) ", row, table, index);

            DataTable dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count != 0)
            {

                re_txt = dt.Rows[0].ItemArray[0].ToString();

            }

            dt.Clear();
            return re_txt;
        }
        //單筆資料欄位加其他條件
        public static string tablesubjectext(string table, string index, string inputid, string row, string othersql)
        {
            //取得資料
            string re_txt = "";
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@inputid", inputid);
                cmd.CommandText = string.Format("SELECT {0} FROM [{1}] WHERE ([{2}] = @inputid {3})", row, table, index, othersql);

                DataTable dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count != 0)
                {

                    re_txt = dt.Rows[0].ItemArray[0].ToString();

                }
                dt.Clear();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return re_txt;
        }

        public static bool SQLAction(string sql)
        {


            SqlConnection conn = getConnection();
            //new SqlConnection(ConfigurationManager.ConnectionStrings["59ConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            return true;

        }



        public static string SQLdosomething(string strTableName, SqlCommand cmd, string action)
        {

            string strReturn = "";
            string strSet = "";
            string strWhere = "";
            string strFields = "";
            string strParas = "";
            SqlParameterCollection paras = cmd.Parameters;
            switch (action)
            {
                case "insert":

                    foreach (SqlParameter item in paras)
                    {
                        strFields += item.ParameterName.Substring(1) + ",";
                        strParas += item.ParameterName + ",";
                    }

                    if (strFields.Length > 1) strFields = strFields.Substring(0, strFields.Length - 1);
                    if (strParas.Length > 1) strParas = strParas.Substring(0, strParas.Length - 1);

                    strReturn = string.Format(@"
                    INSERT INTO {0}
                        ({1})
                    VALUES
                        ({2})
                    ", strTableName, strFields, strParas);

                    break;
                case "del":


                    foreach (SqlParameter item in paras)
                    {
                        if (item.ParameterName.ToLower().StartsWith(strWhereParaKey))
                        {
                            strWhere += string.Format(" and {0} = {1}", item.ParameterName.Substring(strWhereParaKey.Length), item.ParameterName);
                        }
                        else if (item.ParameterName.ToLower().StartsWith(strWhereParaKeyNot))
                        {
                            strWhere += string.Format(" and {0} != {1}", item.ParameterName.Substring(strWhereParaKeyNot.Length), item.ParameterName);
                        }
                    }

                    if (strWhere == "") { throw new Exception("發生錯誤，請聯絡管理員！\n參數傳遞錯誤"); }

                    strReturn = string.Format(@"
                    DELETE FROM {0}
                    WHERE 1=1 {1}
                    ", strTableName, strWhere);



                    break;
                case "update":
                    foreach (SqlParameter item in paras)
                    {
                        if (item.ParameterName.ToLower().StartsWith(strWhereParaKey))
                        {
                            strWhere += string.Format(" and {0} = {1}", item.ParameterName.Substring(strWhereParaKey.Length), item.ParameterName);
                        }
                        else if (item.ParameterName.ToLower().StartsWith(strWhereParaKeyNot))
                        {
                            strWhere += string.Format(" and {0} != {1}", item.ParameterName.Substring(strWhereParaKeyNot.Length), item.ParameterName);
                        }
                        else
                        {
                            strSet += (item.ParameterName.Substring(1) + "=" + item.ParameterName + ",");
                        }
                    }

                    if (strWhere == "") { throw new Exception("發生錯誤，請聯絡管理員！\n參數傳遞錯誤"); }

                    if (strSet.Length > 1) strSet = strSet.Substring(0, strSet.Length - 1);

                    strReturn = string.Format(@"
                    UPDATE {0}
                    SET {1}
                    WHERE 1=1 {2}
                    ", strTableName, strSet, strWhere);

                    break;
                default:
                    break;
            }
            return strReturn;

        }

        #endregion

        #region Do SQL Command

        /// <summary>執行SQL指令，並回傳受影響的列數</summary>
        /// <param name="cmd">The command.</param>
        /// <param name="GetInsertIdentity">是否回傳Identity(when Insert)</param>
        /// <returns>回傳受影響的列數</returns>
        public static int execQuery(SqlCommand cmd, bool GetInsertIdentity = false)
        {
            SqlConnection conn = null;
            int intSuccess = -1;
            try
            {
                conn = getConnection();
                conn.Open();
                cmd.Connection = conn;
                if (!GetInsertIdentity)
                {
                    intSuccess = cmd.ExecuteNonQuery();
                }
                else
                {
                    intSuccess = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                conn.Close();
                intSuccess = -1;
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return intSuccess;
        }

        /// <summary>
        /// 執行無回傳值的SQL指令
        /// </summary>
        /// <param name="cmdTaskFunction"></param>
        public static void execNonQuery(SqlCommand cmd)
        {
            SqlConnection conn = null;

            try
            {
                conn = getConnection();
                conn.Open();
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 執行Transaction指令
        /// </summary>
        /// <param name="listSqls"></param>
        public static void doTranSQL(List<string> listSqls)
        {
            SqlConnection conn = null;
            SqlTransaction dbTrans = null;
            try
            {
                conn = getConnection();
                conn.Open();
                dbTrans = conn.BeginTransaction();

                for (int i = 0; i < listSqls.Count; i++)
                {
                    string strSQL = listSqls[i];
                    SqlCommand cmd = new SqlCommand(strSQL, conn);
                    cmd.ExecuteNonQuery();
                }
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                dbTrans.Rollback();
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 執行Transaction指令
        /// </summary>
        /// <param name="listCmds"></param>
        public static void doTranSQL(List<SqlCommand> listCmds)
        {
            SqlConnection conn = null;
            SqlTransaction dbTrans = null;

            try
            {
                conn = getConnection();
                conn.Open();
                dbTrans = conn.BeginTransaction();
                int intCommandTimeout = 600;//Convert.ToInt32(ConfigurationManager.AppSettings["CommandTimeout"].ToString());

                for (int i = 0; i < listCmds.Count; i++)
                {
                    SqlCommand cmdSql = listCmds[i];
                    cmdSql.CommandTimeout = intCommandTimeout;
                    cmdSql.Connection = conn;
                    cmdSql.Transaction = dbTrans;
                    cmdSql.ExecuteNonQuery();
                }
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                dbTrans.Rollback();
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 執行Transaction指令
        /// </summary>
        /// <param name="cmdMaster"></param>
        /// <param name="listDetails"></param>
        /// <param name="strFKColumn"></param>
        public static object doInsertMD(SqlCommand cmdMaster, List<SqlCommand> listDetails, string strFKColumn)
        {
            object objPK = null;
            SqlConnection conn = null;
            SqlTransaction dbTrans = null;

            try
            {
                int intCommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["CommandTimeout"].ToString());
                conn = getConnection();
                conn.Open();
                dbTrans = conn.BeginTransaction();

                //Insert Master Table
                cmdMaster.CommandTimeout = intCommandTimeout;
                cmdMaster.Connection = conn;
                cmdMaster.Transaction = dbTrans;
                objPK = cmdMaster.ExecuteScalar();

                for (int i = 0; i < listDetails.Count; i++)
                {
                    //Insert Detail Table
                    SqlCommand cmdSql = listDetails[i];
                    cmdSql.Parameters["@" + strFKColumn].Value = objPK;     //在Detail Table輸入Master的PK
                    cmdSql.CommandTimeout = intCommandTimeout;
                    cmdSql.Connection = conn;
                    cmdSql.Transaction = dbTrans;
                    cmdSql.ExecuteNonQuery();
                }
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                dbTrans.Rollback();
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return objPK;
        }

        #endregion

        #region Generate SQL Command

        /// <summary>
        /// 依SqlParameter產生SQL Insert指令
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="isMasterTable">是否需要回傳id值</param>
        /// <param name="cmdTaskFunction">已加入SqlParameter的SqlCommand</param>
        /// <returns></returns>
        public static string genInsertComm(string strTableName, bool isMasterTable, SqlCommand cmd)
        {
            string strReturn = "";
            string strFields = "";
            string strParas = "";

            try
            {
                SqlParameterCollection paras = cmd.Parameters;
                foreach (SqlParameter item in paras)
                {
                    strFields += item.ParameterName.Substring(1) + ",";
                    strParas += item.ParameterName + ",";
                }

                if (strFields.Length > 1) strFields = strFields.Substring(0, strFields.Length - 1);
                if (strParas.Length > 1) strParas = strParas.Substring(0, strParas.Length - 1);

                strReturn = string.Format(@"
                    INSERT INTO {0}
                        ({1})
                    VALUES
                        ({2})
                    ", strTableName, strFields, strParas);

                if (isMasterTable)
                {
                    strReturn += ";SELECT @@Identity";
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }

            return strReturn;
        }

        /// <summary>
        /// 依SqlParameter產生SQL Update指令
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="cmdTaskFunction">已加入SqlParameter的SqlCommand</param>
        /// <returns></returns>
        public static string genUpdateComm(string strTableName, SqlCommand cmd)
        {
            string strReturn = "";
            string strSet = "";
            string strWhere = "";

            try
            {
                SqlParameterCollection paras = cmd.Parameters;
                foreach (SqlParameter item in paras)
                {
                    if (item.ParameterName.ToLower().StartsWith(strWhereParaKey))
                    {
                        strWhere += string.Format(" and {0} = {1}", item.ParameterName.Substring(strWhereParaKey.Length), item.ParameterName);
                    }
                    else if (item.ParameterName.ToLower().StartsWith(strWhereParaKeyNot))
                    {
                        strWhere += string.Format(" and {0} != {1}", item.ParameterName.Substring(strWhereParaKeyNot.Length), item.ParameterName);
                    }
                    else
                    {
                        strSet += (item.ParameterName.Substring(1) + "=" + item.ParameterName + ",");
                    }
                }

                if (strWhere == "") { throw new Exception("發生錯誤，請聯絡管理員！\n參數傳遞錯誤"); }

                if (strSet.Length > 1) strSet = strSet.Substring(0, strSet.Length - 1);

                strReturn = string.Format(@"
                    UPDATE {0}
                    SET {1}
                    WHERE 1=1 {2}
                    ", strTableName, strSet, strWhere);
            }
            catch (Exception ex)
            {
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }

            return strReturn;
        }

        /// <summary>
        /// 依SqlParameter產生SQL Delete指令
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="cmdTaskFunction">已加入SqlParameter的SqlCommand</param>
        /// <returns></returns>
        public static string genDeleteComm(string strTableName, SqlCommand cmd)
        {
            string strReturn = "";
            string strWhere = "";

            try
            {
                SqlParameterCollection paras = cmd.Parameters;
                foreach (SqlParameter item in paras)
                {
                    if (item.ParameterName.ToLower().StartsWith(strWhereParaKey))
                    {
                        strWhere += string.Format(" and {0} = {1}", item.ParameterName.Substring(strWhereParaKey.Length), item.ParameterName);
                    }
                    else if (item.ParameterName.ToLower().StartsWith(strWhereParaKeyNot))
                    {
                        strWhere += string.Format(" and {0} != {1}", item.ParameterName.Substring(strWhereParaKeyNot.Length), item.ParameterName);
                    }
                }

                if (strWhere == "") { throw new Exception("發生錯誤，請聯絡管理員！\n參數傳遞錯誤"); }

                strReturn = string.Format(@"
                    DELETE FROM {0}
                    WHERE 1=1 {1}
                    ", strTableName, strWhere);
            }
            catch (Exception ex)
            {
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }

            return strReturn;
        }

        /// <summary>
        /// 依SqlParameter產生SQL Select指令
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="cmdTaskFunction">已加入SqlParameter的SqlCommand</param>
        /// <returns></returns>
        public static string genSelectComm(string strTableName, SqlCommand cmd)
        {
            string strReturn = "";
            string strWhere = "";

            try
            {
                SqlParameterCollection paras = cmd.Parameters;
                foreach (SqlParameter item in paras)
                {
                    if (item.ParameterName.ToLower().StartsWith(strWhereParaKey))
                    {
                        strWhere += string.Format(" and {0} = {1}", item.ParameterName.Substring(strWhereParaKey.Length), item.ParameterName);
                    }
                    else if (item.ParameterName.ToLower().StartsWith(strWhereParaKeyNot))
                    {
                        strWhere += string.Format(" and {0} != {1}", item.ParameterName.Substring(strWhereParaKeyNot.Length), item.ParameterName);
                    }
                }

                strReturn = string.Format(@"
                    SELECT *
					FROM {0}
                    WHERE 1=1 {1}
                    ", strTableName, strWhere);
            }
            catch (Exception ex)
            {
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }

            return strReturn;
        }

        /// <summary>
        /// 產生SQL WHERE的in指令
        /// </summary>
        /// <param name="cmdTaskFunction"></param>
        /// <param name="strColumnName">欄位名稱</param>
        /// <param name="aryPara">欄位值</param>
        /// <returns></returns>
        public static string genWhereCommIn(ref SqlCommand cmd, string strColumnName, string[] aryPara)
        {
            string strReturn = "";
            string strParas = "";

            try
            {
                for (int i = 0; i < aryPara.Length; i++)
                {
                    strParas += "@" + strColumnName.Substring(strColumnName.IndexOf(".") + 1) + i.ToString() + ",";   //Sql Command

                    cmd.Parameters.AddWithValue("@" + strColumnName.Substring(strColumnName.IndexOf(".") + 1) + i.ToString(), aryPara[i]);
                }

                if (strParas.Length > 0)
                {
                    strParas = strParas.Substring(0, strParas.Length - 1);

                    strReturn = string.Format(" and {0} in ({1})", strColumnName, strParas);   //Sql Command
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }

            return strReturn;
        }

        /// <summary>
        /// 產生SQL WHERE的in指令
        /// </summary>
        /// <param name="cmdTaskFunction"></param>
        /// <param name="strColumnName">欄位名稱</param>
        /// <param name="listPara">欄位值</param>
        /// <returns></returns>
        public static string genWhereCommIn(ref SqlCommand cmd, string strColumnName, List<string> listPara)
        {
            string strReturn = "";
            string strParas = "";

            try
            {
                if (listPara.Count == 0) { throw new Exception("發生錯誤，請聯絡管理員！\n參數傳遞錯誤"); }

                for (int i = 0; i < listPara.Count; i++)
                {
                    strParas += "@" + strColumnName.Substring(strColumnName.IndexOf(".") + 1) + i.ToString() + ",";   //Sql Command

                    cmd.Parameters.AddWithValue("@" + strColumnName.Substring(strColumnName.IndexOf(".") + 1) + i.ToString(), listPara[i]);
                }

                if (strParas.Length > 0)
                {
                    strParas = strParas.Substring(0, strParas.Length - 1);

                    strReturn = string.Format(" and {0} in ({1})", strColumnName, strParas);   //Sql Command
                }
            }
            catch (Exception ex)
            {
                //ExceptionHandler.logSysException(ex);
                throw ex;
            }

            return strReturn;
        }

        /// <summary>
        /// 產生更新列印次數的指令
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="cmdTaskFunction"></param>
        /// <param name="strColName"></param>
        /// <returns></returns>
        public static string genUpdPrinted(string strTableName, SqlCommand cmd, string strColName)
        {
            string strResult = "";
            string strWhere = "";

            try
            {
                SqlParameterCollection paras = cmd.Parameters;
                foreach (SqlParameter item in paras)
                {
                    if (item.ParameterName.ToLower().StartsWith(strWhereParaKey))
                    {
                        strWhere += string.Format(" and {0} = {1}", item.ParameterName.Substring(strWhereParaKey.Length), item.ParameterName);
                    }
                    else if (item.ParameterName.ToLower().StartsWith(strWhereParaKeyNot))
                    {
                        strWhere += string.Format(" and {0} != {1}", item.ParameterName.Substring(strWhereParaKeyNot.Length), item.ParameterName);
                    }
                }

                //避免Where條件錯誤，造成全部資料更新
                if (strWhere == "") { throw new Exception("發生錯誤，請聯絡管理員！\n參數傳遞錯誤"); }

                strResult = string.Format(@"
				update {0}
				set {1} = ISNULL({1}, 0) + 1
				where 1=1 {2}", strTableName, strColName, strWhere);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strResult;
        }

        /// <summary>
        /// 產生更新列印檔名的指令(注意，檔名已存在的不會更新)
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="cmdTaskFunction"></param>
        /// <returns></returns>
        public static string genUpdFileName(string strTableName, SqlCommand cmd)
        {
            string strResult = "";
            string strWhere = "";
            string strSet = "";

            try
            {
                SqlParameterCollection paras = cmd.Parameters;
                foreach (SqlParameter item in paras)
                {
                    if (item.ParameterName.ToLower().StartsWith(strWhereParaKey))
                    {
                        strWhere += string.Format(" and {0} = {1}", item.ParameterName.Substring(strWhereParaKey.Length), item.ParameterName);
                    }
                    else if (item.ParameterName.ToLower().StartsWith(strWhereParaKeyNot))
                    {
                        strWhere += string.Format(" and {0} != {1}", item.ParameterName.Substring(strWhereParaKeyNot.Length), item.ParameterName);
                    }
                    else
                    {
                        strSet += string.Format(" {0} = {1},", item.ParameterName.Substring(1), item.ParameterName);
                        strWhere += string.Format(" and isnull({0}, '') = ''", item.ParameterName.Substring(1));
                    }
                }
                if (strSet.Length > 1) strSet = strSet.Substring(0, strSet.Length - 1);

                if (strWhere == "") { throw new Exception("發生錯誤，請聯絡管理員！\n參數傳遞錯誤"); }

                strResult = string.Format(@"
				update {0}
				set {1}
				where 1=1 {2}", strTableName, strSet, strWhere);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strResult;
        }

        #endregion
    }
}