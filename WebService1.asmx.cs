﻿using BarcodeLib;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Web.Services;


namespace JuFanAPI
{
    /// <summary>
    ///WebService1 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        internal const int LOCALE_SYSTEM_DEFAULT = 0x0800;
        internal const int LCMAP_SIMPLIFIED_CHINESE = 0x02000000;
        internal const int LCMAP_TRADITIONAL_CHINESE = 0x04000000;

        /// <summary>
        /// 使用OS的kernel.dll做為簡繁轉換工具，只要有裝OS就可以使用，不用額外引用dll，但只能做逐字轉換，無法進行詞意的轉換
        /// <para>所以無法將電腦轉成計算機</para>
        /// </summary>
        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern int LCMapString(int Locale, int dwMapFlags, string lpSrcStr, int cchSrc, [Out] string lpDestStr, int cchDest);

        private string project_name = "JUNFU_APP_WebService";
        private int db_Timeout = 30; //30秒
        private string db_junfu_sql_conn_str = ConfigurationManager.ConnectionStrings["dbSqlJunfu_ConnStr"].ConnectionString;

        #region EDI API
        /// <summary>
        /// EDI API
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "EDI 建立託運單", EnableSession = true)]//(Description = "EDI API", EnableSession = true)
        public string EDIAPI(string Token, string getJson)
        {   //可更新為將Token記錄在local storage 

            DateTime start_time = DateTime.Now;

            string strAppRequest = "";
            string key = "B69CCA65B4D0B705E37080676E5F5DBBF4EA0E7A0EBD7C60CCE2024E218A53BB";
            if (Token == "")
            {
                strAppRequest = JsonConvert.SerializeObject(new { resultcode = false, resultdesc = "請輸入Token" }, Newtonsoft.Json.Formatting.Indented);
                return strAppRequest;
            }
            else
            {
                //string[] split = Token.Split('.');
                // if (split.Length < 3)
                // {
                //     strAppRequest = JsonConvert.SerializeObject(new { resultcode = false, resultdesc = "輸入錯誤的Token格式" }, Newtonsoft.Json.Formatting.Indented);
                //     return strAppRequest;
                // }
                // else {
                //string iv = split[0];
                //string encrypt = split[1];
                //string signature = split[2];
                string encrypt = Token;
                DataTable dt = new DataTable();
                //檢查簽章是否正確
                //if (signature != ComputeHMACSHA256(iv + "." + encrypt, key.Substring(0, 64)))
                //{
                //    strAppRequest = JsonConvert.SerializeObject(new { resultcode = false, resultdesc = "輸入簽章錯誤" }, Newtonsoft.Json.Formatting.Indented);
                //    return strAppRequest;
                //}
                //使用 AES 解密 Payload
                //var base64 = AESDecrypt(encrypt, key.Substring(0, 16), iv);
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(encrypt));
                var payload = JsonConvert.DeserializeObject<Payload>(json);

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@account_code", payload.info.account_code);
                    cmd.CommandText = @"select * from tbAccounts where account_code = @account_code";
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt.Rows.Count <= 0)
                    {
                        strAppRequest = JsonConvert.SerializeObject(new { resultcode = false, resultdesc = "輸入簽章錯誤" }, Newtonsoft.Json.Formatting.Indented);
                        return strAppRequest;
                    }
                }
                //檢查是否過期
                if (payload.exp < Convert.ToInt32(
                    (DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds))
                {
                    strAppRequest = JsonConvert.SerializeObject(new { resultcode = false, resultdesc = "輸入簽章逾期請重新申請簽章" }, Newtonsoft.Json.Formatting.Indented);
                    return strAppRequest;
                }
                //}                
            }


            string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataTable dtAppRequest = new DataTable();
            Boolean resultcode = true;
            string error_msg = string.Empty;
            string driver_name = string.Empty;
            string supplier_code = string.Empty;
            string type = "0";  // 0: 峻富一般司機  1:外車司機
            string receive_city = string.Empty;
            string receive_area = string.Empty;

            string send_city = string.Empty;
            string send_area = string.Empty;
            string ShuttleStationCode = string.Empty;
            if (getJson.Equals(""))
            {
                DateTime end_time = DateTime.Now;
                resultcode = false;
                error_msg = "無Json 檔";
                writeLogForEDI("call", "EDIAPI", "", error_msg, "", "", resultcode, start_time, end_time);
            }
            else
            {
                JavaScriptSerializer js = new JavaScriptSerializer();

                List<tcDeliveryRequests> modelDy = js.Deserialize<List<tcDeliveryRequests>>(getJson);

                if (modelDy != null && modelDy.Count > 0)
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select stop_shipping_code from tbCustomers where customer_code = @customer_code";
                        cmd.Parameters.AddWithValue("@customer_code", modelDy[0].customercode);

                        DataTable customer = dbAdapter.getDataTable(cmd);
                        if (customer != null && customer.Rows.Count > 0)
                        {
                            if (customer.Rows[0]["stop_shipping_code"].ToString() != "0")
                            {
                                DateTime end_time = DateTime.Now;
                                resultcode = false;
                                error_msg = "the account code is not active";
                                writeLogForEDI("call", "EDIAPI", getJson, error_msg, "", "", resultcode, start_time, end_time);
                                goto EndLabel;
                            }
                        }
                    }
                }

                foreach (tcDeliveryRequests temp in modelDy)
                {
                    int ttpieces = 0;
                    int ttplates = 0;
                    int ttcbm = 0;
                    int ttcollection_money = 0;
                    int ttarrive_to_pay = 0;
                    int ttarrive_to_append = 0;
                    int Reportfee = 0;
                    int remote_fee = 0;
                    int request_id = 0;
                    int supplier_fee = 0;  //配送費用
                    int cscetion_fee = 0;  //C配運價
                    int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)

                    string productId = string.Empty;
                    string specCodeId = string.Empty;
                    string CBM = string.Empty;


                    string receiveraddressCity = string.Empty;
                    string receiveraddressTown = string.Empty;
                    string receiveraddressRoad = string.Empty;
                    string receiveraddressStationCode = string.Empty;
                    string receiveraddressPostZip3 = string.Empty;
                    string receiveraddressStackCode = string.Empty;
                    string receiveraddressMotorcycleDriverCode = string.Empty;
                    string receiveraddressSalesDriverCode = string.Empty;
                    string receiveraddressStationName = string.Empty;
                    string receiveraddressShuttleStationCode = string.Empty;
                    int? receiveraddressSpecialAreaFee = 0;
                    string receiveraddressSpecialAreaId = string.Empty;
                    string realReceiverCity = string.Empty;
                    string realReceiverTown = string.Empty;
                    string realReceiverRoad = string.Empty;



                    string senderaddressCity = string.Empty;
                    string senderaddressTown = string.Empty;
                    string senderaddressRoad = string.Empty;
                    string senderaddressStationCode = string.Empty;
                    string senderaddressPostZip3 = string.Empty;
                    string senderaddressStackCode = string.Empty;
                    string senderaddressMotorcycleDriverCode = string.Empty;
                    string senderaddressSalesDriverCode = string.Empty;
                    string senderaddressStationName = string.Empty;




                    using (SqlCommand cmd = new SqlCommand())
                    {
                        DataTable dtGetDistributor = new DataTable();

                        if (temp.checknumber == "")
                        {
                            DateTime end_time2 = DateTime.Now;
                            resultcode = false;
                            error_msg = "未輸入貨號，請洽客服人員";
                            writeLogForEDI("call", "EDIAPI", getJson, error_msg, temp.checknumber, temp.customercode, resultcode, start_time, end_time2);
                            goto EndLabel;
                        }

                        cmd.Parameters.AddWithValue("@check_number", temp.checknumber);                             //託運單號(貨號) 現階段長度10~20碼
                        cmd.Parameters.AddWithValue("@Less_than_truckload", 1);                                      //0：棧板運輸  1：零擔運輸
                                                                                                                     //cmd.Parameters.AddWithValue("@Distributor", dlDistributor.SelectedValue);                   //配送商(全速配、宅配通)
                        cmd.Parameters.AddWithValue("@CbmSize", temp.CbmSize);                                       //材積大小
                        cmd.Parameters.AddWithValue("@pricing_type", "02");                                          //計價模式 (01:論板、02:論件、03論才、04論小板)
                        cmd.Parameters.AddWithValue("@customer_code", temp.customercode);                            //客代編號
                        cmd.Parameters.AddWithValue("@order_number", temp.ordernumber);                              //訂單號碼
                        cmd.Parameters.AddWithValue("@check_type", "001");                                           //託運類別
                        cmd.Parameters.AddWithValue("@receive_customer_code", temp.receivecustomercode);             //收貨人編號

                        //如果有代收貨款金額，傳票類別一律改為代收貨款

                        string subpoenacategory = temp.subpoenacategory;
                        if (temp.collectionmoney.Length > 0 && !temp.collectionmoney.Equals("0"))
                        {
                            subpoenacategory = "41"; //代收貨款
                        }
                        else
                        {
                            subpoenacategory = "11"; //元付
                        }

                        cmd.Parameters.AddWithValue("@subpoena_category", subpoenacategory);                    //傳票類別
                        cmd.Parameters.AddWithValue("@collection_money", temp.collectionmoney);                      //代收貨款

                        if (string.IsNullOrEmpty(temp.collectionmoney))
                        {
                            temp.collectionmoney = "0";

                        }

                        if (Int32.Parse(temp.collectionmoney) > 20000)
                        {
                            strAppRequest = JsonConvert.SerializeObject(new { resultcode = false, resultdesc = "代收貨款超過20,000上限" }, Newtonsoft.Json.Formatting.Indented);
                            return strAppRequest;
                        }
                        cmd.Parameters.AddWithValue("@receive_tel1", temp.receivetel1);                              //電話1
                        cmd.Parameters.AddWithValue("@receive_tel2", temp.receivetel2);                              //電話2
                        cmd.Parameters.AddWithValue("@receive_contact", temp.receivecontact);                        //收件人    


                        var SpecialAreaAddressinfo = new SpecialAreaManage();

                        SpecialAreaAddressinfo = PreGetSpecialAreaFee(temp.ReceiverAddress, temp.customercode, "2");

                        receiveraddressCity = string.IsNullOrEmpty(SpecialAreaAddressinfo.City) ? string.Empty : SpecialAreaAddressinfo.City;
                        receiveraddressTown = string.IsNullOrEmpty(SpecialAreaAddressinfo.Town) ? string.Empty : SpecialAreaAddressinfo.Town;
                        receiveraddressStationCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.StationCode) ? string.Empty : SpecialAreaAddressinfo.StationCode;
                        receiveraddressShuttleStationCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.ShuttleStationCode) ? string.Empty : SpecialAreaAddressinfo.ShuttleStationCode;
                        receiveraddressSpecialAreaFee = SpecialAreaAddressinfo.Fee == null ? 0 : SpecialAreaAddressinfo.Fee;
                        receiveraddressSpecialAreaId = string.IsNullOrEmpty(SpecialAreaAddressinfo.id.ToString()) ? string.Empty : SpecialAreaAddressinfo.id.ToString();
                        receiveraddressMotorcycleDriverCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.MotorcycleDriverCode) ? string.Empty : SpecialAreaAddressinfo.MotorcycleDriverCode;
                        receiveraddressSalesDriverCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.SalesDriverCode) ? string.Empty : SpecialAreaAddressinfo.SalesDriverCode;

                        if (string.IsNullOrEmpty(SpecialAreaAddressinfo.StationCode))
                        {

                            var receiveAddressinfo = new AddressParsingInfo();
                            receiveAddressinfo = PreGetAddressParsing(temp.ReceiverAddress, temp.customercode, "2");

                            receiveraddressCity = string.IsNullOrEmpty(receiveAddressinfo.City) ? string.Empty : receiveAddressinfo.City;
                            receiveraddressTown = string.IsNullOrEmpty(receiveAddressinfo.Town) ? string.Empty : receiveAddressinfo.Town;
                            receiveraddressStationCode = string.IsNullOrEmpty(receiveAddressinfo.StationCode) ? string.Empty : receiveAddressinfo.StationCode;
                            receiveraddressShuttleStationCode = string.IsNullOrEmpty(receiveAddressinfo.ShuttleStationCode) ? string.Empty : receiveAddressinfo.ShuttleStationCode;
                            receiveraddressMotorcycleDriverCode = string.IsNullOrEmpty(receiveAddressinfo.MotorcycleDriverCode) ? string.Empty : receiveAddressinfo.MotorcycleDriverCode;
                            receiveraddressSalesDriverCode = string.IsNullOrEmpty(receiveAddressinfo.SalesDriverCode) ? string.Empty : receiveAddressinfo.SalesDriverCode;

                            if (string.IsNullOrEmpty(receiveAddressinfo.StationCode))
                            {
                                //DateTime end_time4 = DateTime.Now;
                                //resultcode = false;
                                //error_msg = "收件地址有誤，請洽客服人員";
                                //writeLogForEDI("call", "EDIAPI", getJson, error_msg, temp.checknumber, temp.customercode, resultcode, start_time, end_time4);
                                //goto EndLabel;

                                //原拆收件地址
                                using (SqlCommand cmd_addr = new SqlCommand())
                                {
                                    cmd_addr.Parameters.AddWithValue("@address", temp.ReceiverAddress);
                                    cmd_addr.CommandText = "dbo.usp_AddrFormat";
                                    cmd_addr.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        DataTable dt_addr = dbAdapter.getDataTable(cmd_addr);
                                        if (dt_addr != null && dt_addr.Rows.Count > 0)
                                        {
                                            receiveraddressCity = dt_addr.Rows[0]["city"].ToString();
                                            receiveraddressTown = dt_addr.Rows[0]["area"].ToString();
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }

                                using (SqlCommand uspcmd = new SqlCommand())
                                {
                                    uspcmd.CommandText = @"usp_GetDistributor";
                                    uspcmd.CommandType = CommandType.StoredProcedure;
                                    uspcmd.Parameters.AddWithValue("@receive_city", receiveraddressCity);
                                    uspcmd.Parameters.AddWithValue("@receive_area", receiveraddressTown);
                                    dtGetDistributor = dbAdapter.getDataTable(uspcmd);

                                    receiveraddressStationCode = dtGetDistributor.Rows[0]["area_arrive_code"].ToString();
                                }

                            }
                        }

                        var sendAddressInfo = new AddressParsingInfo();
                        sendAddressInfo = PreGetAddressParsing(temp.SenderAddress, temp.customercode, "2");

                        senderaddressCity = string.IsNullOrEmpty(sendAddressInfo.City) ? string.Empty : sendAddressInfo.City;
                        senderaddressTown = string.IsNullOrEmpty(sendAddressInfo.Town) ? string.Empty : sendAddressInfo.Town;
                        senderaddressRoad = string.IsNullOrEmpty(sendAddressInfo.Road) ? string.Empty : sendAddressInfo.Road;
                        senderaddressStationCode = string.IsNullOrEmpty(sendAddressInfo.StationCode) ? string.Empty : sendAddressInfo.StationCode;
                        senderaddressMotorcycleDriverCode = string.IsNullOrEmpty(sendAddressInfo.sendMotorcycleDriverCode) ? string.Empty : sendAddressInfo.sendMotorcycleDriverCode;
                        senderaddressSalesDriverCode = string.IsNullOrEmpty(sendAddressInfo.sendSalesDriverCode) ? string.Empty : sendAddressInfo.sendSalesDriverCode;

                        if (string.IsNullOrEmpty(sendAddressInfo.StationCode))
                        {
                            //DateTime end_time3 = DateTime.Now;
                            //resultcode = false;
                            //error_msg = "寄件地址有誤，請洽客服人員";
                            //writeLogForEDI("call", "EDIAPI", getJson, error_msg, temp.checknumber, temp.customercode, resultcode, start_time, end_time3);
                            //goto EndLabel;

                            //原拆寄件地址
                            using (SqlCommand cmd_addr = new SqlCommand())
                            {
                                cmd_addr.Parameters.AddWithValue("@address", temp.SenderAddress);
                                cmd_addr.CommandText = "dbo.usp_AddrFormat";
                                cmd_addr.CommandType = CommandType.StoredProcedure;
                                try
                                {
                                    DataTable dt_addr = dbAdapter.getDataTable(cmd_addr);
                                    if (dt_addr != null && dt_addr.Rows.Count > 0)
                                    {
                                        senderaddressCity = dt_addr.Rows[0]["city"].ToString();
                                        senderaddressTown = dt_addr.Rows[0]["area"].ToString();
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }



                        }


                        cmd.Parameters.AddWithValue("@SendCode", senderaddressStationCode);
                        cmd.Parameters.AddWithValue("@SendSD", senderaddressSalesDriverCode);
                        cmd.Parameters.AddWithValue("@SendMD", senderaddressMotorcycleDriverCode);
                        cmd.Parameters.AddWithValue("@ReceiveCode", receiveraddressStationCode);
                        cmd.Parameters.AddWithValue("@ReceiveSD", receiveraddressSalesDriverCode);
                        cmd.Parameters.AddWithValue("@ReceiveMD", receiveraddressMotorcycleDriverCode);



                        //temp.SenderAddress = send_city != "" ? temp.SenderAddress.Replace(send_city, "") : temp.SenderAddress;
                        //temp.SenderAddress = send_area != "" ? temp.SenderAddress.Replace(send_area, "") : temp.SenderAddress;

                        //cmd.Parameters.AddWithValue("@send_city", send_city);                //寄件人地址-縣市
                        //cmd.Parameters.AddWithValue("@send_area", send_area);                //寄件人地址-鄉鎮市區
                        //cmd.Parameters.AddWithValue("@send_address", temp.SenderAddress.Length > 50 ? temp.SenderAddress.Substring(0, 50) : temp.SenderAddress); //寄件人地址-號街巷弄號
                        senderaddressRoad = senderaddressCity != "" ? temp.SenderAddress.Replace(senderaddressCity, "") : temp.SenderAddress;
                        senderaddressRoad = senderaddressTown != "" ? senderaddressRoad.Replace(senderaddressTown, "") : senderaddressRoad;

                        cmd.Parameters.AddWithValue("@send_city", senderaddressCity);                //寄件人地址-縣市
                        cmd.Parameters.AddWithValue("@send_area", senderaddressTown);                //寄件人地址-鄉鎮市區
                        cmd.Parameters.AddWithValue("@send_address", senderaddressRoad.Length > 50 ? senderaddressRoad.Substring(0, 50) : senderaddressRoad); //寄件人地址-號街巷弄號


                        //temp.ReceiverAddress = receive_city != "" ? temp.ReceiverAddress.Replace(receive_city, "") : temp.ReceiverAddress;
                        //temp.ReceiverAddress = receive_area != "" ? temp.ReceiverAddress.Replace(receive_area, "") : temp.ReceiverAddress;
                        receiveraddressRoad = receiveraddressCity != "" ? temp.ReceiverAddress.Replace(receiveraddressCity, "") : temp.ReceiverAddress;
                        receiveraddressRoad = receiveraddressTown != "" ? receiveraddressRoad.Replace(receiveraddressTown, "") : receiveraddressRoad;

                        using (SqlCommand cmd_realaddr = new SqlCommand())
                        {
                            cmd_realaddr.Parameters.AddWithValue("@address", temp.ReceiverAddress);
                            cmd_realaddr.CommandText = "dbo.usp_AddrFormat";
                            cmd_realaddr.CommandType = CommandType.StoredProcedure;
                            try
                            {
                                DataTable dt_realaddr = dbAdapter.getDataTable(cmd_realaddr);
                                if (dt_realaddr != null && dt_realaddr.Rows.Count > 0)
                                {
                                    realReceiverCity = dt_realaddr.Rows[0]["city"].ToString();
                                    realReceiverTown = dt_realaddr.Rows[0]["area"].ToString();
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        realReceiverRoad = realReceiverCity != "" ? temp.ReceiverAddress.Replace(realReceiverCity, "") : temp.ReceiverAddress;
                        realReceiverRoad = realReceiverTown != "" ? realReceiverRoad.Replace(realReceiverTown, "") : realReceiverRoad;


                        cmd.Parameters.AddWithValue("@receive_city", realReceiverCity);                                  //收件地址-縣市
                        cmd.Parameters.AddWithValue("@receive_area", realReceiverTown);                                  //收件地址-鄉鎮市區
                        cmd.Parameters.AddWithValue("@receive_address", realReceiverRoad.Length > 50 ? realReceiverRoad.Substring(0, 50) : realReceiverRoad);

                        //接泊碼                        
                        ShuttleStationCode = receiveraddressShuttleStationCode == null ? "" : receiveraddressShuttleStationCode;
                        cmd.Parameters.AddWithValue("@ShuttleStationCode", ShuttleStationCode);

                        //特服區費用
                        cmd.Parameters.AddWithValue("@SpecialAreaFee", receiveraddressSpecialAreaFee);

                        //特服區id

                        cmd.Parameters.AddWithValue("@SpecialAreaId", receiveraddressSpecialAreaId);


                        //cmd.Parameters.AddWithValue("@receive_city", receiveraddress.City);                                  //收件地址-縣市
                        //cmd.Parameters.AddWithValue("@receive_area", receiveraddress.Town);                                  //收件地址-鄉鎮市區
                        //cmd.Parameters.AddWithValue("@receive_address", receiveraddress.Road);                       //收件地址-路街巷弄號

                        //件數
                        string pieces = temp.pieces;
                        if (pieces == null || pieces == "")
                        {
                            pieces = temp.plates;
                        }

                        cmd.Parameters.AddWithValue("@plates", temp.plates);
                        cmd.Parameters.AddWithValue("@pieces", pieces);

                        //取得producttype
                        SqlCommand cmd8 = new SqlCommand();
                        cmd8.Parameters.AddWithValue("@customer_code", temp.customercode);
                        cmd8.CommandText = cmd.CommandText = @"select product_type,is_new_customer from tbcustomers where customer_code = @customer_code";
                        DataTable dt6 = dbAdapter.getDataTable(cmd8);
                        string get_prodcut_type = dt6.Rows[0]["product_type"].ToString();
                        string is_new_customer = dt6.Rows[0]["is_new_customer"].ToString();

                        if (get_prodcut_type != null)
                        {
                            if ((get_prodcut_type == "2" || get_prodcut_type == "3") && receiveraddressStationCode == "95")
                            {
                                DateTime end_time4 = DateTime.Now;
                                resultcode = false;
                                error_msg = "收件地址非於服務範圍內，請洽客服人員";
                                writeLogForEDI("call", "EDIAPI", getJson, error_msg, temp.checknumber, temp.customercode, resultcode, start_time, end_time4);
                                goto EndLabel;
                            }
                        }

                        //寫入產品類型
                        if (get_prodcut_type != null)
                        {
                            if (get_prodcut_type == "1")
                            {
                                if (Convert.ToInt32(pieces) > 1)
                                {
                                    if (is_new_customer == "True")
                                    {
                                        productId = "CM000030";
                                    }
                                    else
                                    {
                                        productId = "CM000036";
                                    }
                                    if (temp.CbmSize == "1")
                                    {
                                        specCodeId = "S060";
                                    }
                                    else if (temp.CbmSize == "2")
                                    {
                                        specCodeId = "S090";
                                    }
                                    else if (temp.CbmSize == "6")
                                    {
                                        specCodeId = "S110";
                                    }
                                    else if (temp.CbmSize == "3")
                                    {
                                        specCodeId = "S120";
                                    }
                                    else if (temp.CbmSize == "4")
                                    {
                                        specCodeId = "S150";
                                    }
                                    else
                                    {
                                        specCodeId = "S090";
                                    }

                                }
                                else
                                {
                                    if (is_new_customer == "True")
                                    {
                                        productId = "CM000030";
                                    }
                                    else
                                    {
                                        productId = "CM000036";
                                    }
                                    if (temp.CbmSize == "1")
                                    {
                                        specCodeId = "S060";
                                    }
                                    else if (temp.CbmSize == "2")
                                    {
                                        specCodeId = "S090";
                                    }
                                    else if (temp.CbmSize == "6")
                                    {
                                        specCodeId = "S110";
                                    }
                                    else if (temp.CbmSize == "3")
                                    {
                                        specCodeId = "S120";
                                    }
                                    else if (temp.CbmSize == "4")
                                    {
                                        specCodeId = "S150";
                                    }
                                    else
                                    {
                                        specCodeId = "S090";
                                    }
                                }
                            }
                            else if (get_prodcut_type == "2")
                            {
                                productId = "PS000031";
                                specCodeId = "B003";
                            }
                            else if (get_prodcut_type == "3")
                            {
                                productId = "PS000034";
                                specCodeId = "X001";

                            }
                            else if (get_prodcut_type == "6")
                            {
                                productId = "CM000043";

                            }
                        }

                        cmd.Parameters.AddWithValue("@ProductId", productId);
                        cmd.Parameters.AddWithValue("@SpecCodeId", specCodeId);


                        //寫入結算材積計算table
                        try
                        {

                            if (get_prodcut_type != null)
                            {
                                if (get_prodcut_type == "1")
                                {
                                    if (temp.CbmSize == "1")
                                    {
                                        CBM = "S060";
                                    }
                                    else if (temp.CbmSize == "2")
                                    {
                                        CBM = "S090";
                                    }
                                    else if (temp.CbmSize == "6")
                                    {
                                        CBM = "S110";
                                    }
                                    else if (temp.CbmSize == "3")
                                    {
                                        CBM = "S120";
                                    }
                                    else if (temp.CbmSize == "4")
                                    {
                                        CBM = "S150";
                                    }
                                    else
                                    {
                                        CBM = "S090";
                                    }
                                }
                                else if (get_prodcut_type == "2")
                                {
                                    CBM = "B003";
                                }
                                else if (get_prodcut_type == "3")
                                {
                                    CBM = "X003";
                                }
                                else if (get_prodcut_type == "6")
                                {
                                    CBM = "S150";
                                }

                            }

                            SqlCommand CBMDetailLogCommand = new SqlCommand(); // insert into pick_up_request_log
                            CBMDetailLogCommand.Parameters.AddWithValue("@ComeFrom", "1");
                            CBMDetailLogCommand.Parameters.AddWithValue("@CheckNumber", temp.checknumber);
                            CBMDetailLogCommand.Parameters.AddWithValue("@CBM", CBM);
                            CBMDetailLogCommand.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                            CBMDetailLogCommand.Parameters.AddWithValue("@CreateUser", temp.customercode);
                            CBMDetailLogCommand.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, CBMDetailLogCommand);
                            dbAdapter.execNonQuery(CBMDetailLogCommand);

                        }
                        catch (Exception e)
                        {


                        };



                        SqlCommand sqlCommand = new SqlCommand(); // insert into pick_up_request_log
                        sqlCommand.Parameters.AddWithValue("@cdate", DateTime.Now);
                        sqlCommand.Parameters.AddWithValue("@udate", DateTime.Now);
                        sqlCommand.Parameters.AddWithValue("@request_customer_code", temp.customercode);
                        sqlCommand.Parameters.AddWithValue("@pick_up_pieces", pieces);
                        sqlCommand.CommandText = dbAdapter.genInsertComm("pick_up_request_log", false, sqlCommand);
                        dbAdapter.execNonQuery(sqlCommand);

                        cmd.Parameters.AddWithValue("@send_contact", temp.sendcontact.Length > 40 ? temp.sendcontact.Substring(0, 40) : temp.sendcontact); //寄件人
                        cmd.Parameters.AddWithValue("@send_tel", temp.sendtel);                                    //寄件人電話

                        cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
                        cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
                        cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
                                                                                                                      //cmd.Parameters.AddWithValue("@arrive_mobile", arrive_mobile.Text.ToString());                 //收件人-手機1  
                        cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
                                                                                                                      //cmd.Parameters.AddWithValue("@invoice_memo", invoice_memo.SelectedValue.ToString());        //備註
                        cmd.Parameters.AddWithValue("@invoice_desc", temp.invoicedesc.Length > 200 ? temp.invoicedesc.Substring(0, 200) : temp.invoicedesc);   //說明
                                                                                                                                                               //cmd.Parameters.AddWithValue("@product_category", product_category.SelectedValue.ToString());  //商品種類

                        //發送站代號
                        //if (dtGetDistributor.Rows[0]["supplier_code"].ToString() != "*9聯運")
                        cmd.Parameters.AddWithValue("@supplier_code", temp.suppliercode);
                        //發送站名稱
                        cmd.Parameters.AddWithValue("@supplier_name", "");
                        //到著站簡碼
                        cmd.Parameters.AddWithValue("@area_arrive_code", receiveraddressStationCode);

                        ////發送站代號
                        //if (senderaddress.StationCode.ToString() != "*9聯運")
                        //    cmd.Parameters.AddWithValue("@supplier_code", temp.suppliercode);
                        ////發送站名稱
                        //cmd.Parameters.AddWithValue("@supplier_name", "");
                        ////到著站簡碼
                        //cmd.Parameters.AddWithValue("@area_arrive_code", receiveraddress.StationCode.ToString());
                        //cmd.Parameters.AddWithValue("@ArticleNumber", ArticleNumber.Text);  //產品編號
                        //cmd.Parameters.AddWithValue("@SendPlatform", SendPlatform.Text);    //出貨平台
                        //cmd.Parameters.AddWithValue("@ArticleName", ArticleName.Text);      //品名

                        DateTime date;
                        cmd.Parameters.AddWithValue("@arrive_assign_date", DateTime.TryParse(temp.arriveassigndate, out date) ? (object)date : DBNull.Value);                  //指定日
                        #region 派送時段
                        Regex periodreg = new Regex("-");
                        string time_period = "";
                        if (periodreg.IsMatch(temp.timeperiod))
                        {
                            int s = temp.timeperiod.IndexOf("-", 0, temp.timeperiod.Length);
                            time_period = temp.timeperiod.Substring(s + 1, 1);
                        }
                        else
                        {
                            time_period = "不指定";
                        }
                        #endregion
                        cmd.Parameters.AddWithValue("@time_period", time_period);            //時段
                                                                                             //是否回單     
                        if (temp.receiptflag == "Y")
                            cmd.Parameters.AddWithValue("@receipt_flag", 1);
                        else
                            cmd.Parameters.AddWithValue("@receipt_flag", 0);
                        //來回件  
                        if (temp.roundtrip == "Y")
                            cmd.Parameters.AddWithValue("@round_trip", 1);
                        else
                            cmd.Parameters.AddWithValue("@round_trip", 0);


                        //產品編號
                        cmd.Parameters.AddWithValue("@ArticleNumber", temp.ArticleNumber);
                        //出貨平台
                        cmd.Parameters.AddWithValue("@SendPlatform", temp.SendPlatform);
                        //品名
                        cmd.Parameters.AddWithValue("@ArticleName", temp.ArticleName);

                        cmd.Parameters.AddWithValue("@DeliveryType", "D");
                        //更新人員                                    
                        cmd.Parameters.AddWithValue("@uuser", "skyeyes");
                        //更新時間                         
                        cmd.Parameters.AddWithValue("@udate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@print_flag", 0);


                        //預購袋數量要減1
                        SqlCommand cmd5 = new SqlCommand();
                        cmd5.Parameters.AddWithValue("@customer_code", temp.customercode);
                        cmd5.CommandText = cmd.CommandText = @"select product_type from tbcustomers where customer_code = @customer_code";
                        DataTable dt = dbAdapter.getDataTable(cmd5);
                        string prodcut_type = dt.Rows[0]["product_type"].ToString();

                        if (prodcut_type == "2" || prodcut_type == "3")
                        {
                            SqlCommand cmd6 = new SqlCommand();
                            cmd6.Parameters.AddWithValue("@customer_code", temp.customercode);
                            cmd6.CommandText = cmd.CommandText = @"select * from Customer_checknumber_setting where customer_code = @customer_code order by id desc";
                            DataTable dt2 = dbAdapter.getDataTable(cmd6);

                            if (dt2.Rows.Count == 0)
                            {
                                DateTime end_time5 = DateTime.Now;
                                resultcode = false;
                                error_msg = "未購買預購袋/超值箱，請洽客服人員";
                                writeLogForEDI("call", "EDIAPI", getJson, error_msg, temp.checknumber, temp.customercode, resultcode, start_time, end_time5);
                                goto EndLabel;
                            }

                            int totalCount = Convert.ToInt32(dt2.Rows[0]["total_count"]);
                            int usedCount = Convert.ToInt32(dt2.Rows[0]["used_count"]);
                            int remain = totalCount - usedCount;

                            if (remain < 1)
                            {
                                DateTime end_time6 = DateTime.Now;
                                resultcode = false;
                                error_msg = "預購袋/超值箱數量不足，請洽客服人員";
                                writeLogForEDI("call", "EDIAPI", getJson, error_msg, temp.checknumber, temp.customercode, resultcode, start_time, end_time6);
                                goto EndLabel;
                            }

                            if (usedCount.ToString() != null)
                            {
                                usedCount = usedCount + 1;

                                DataTable dt3 = new DataTable();
                                SqlCommand cmd7 = new SqlCommand();
                                cmd7.Parameters.AddWithValue("@used_count", usedCount);
                                cmd7.Parameters.AddWithValue("@update_date", DateTime.Now);
                                cmd7.Parameters.AddWithValue("@customer_code", temp.customercode);
                                cmd7.CommandText = "update Customer_checknumber_setting set used_count = @used_count  where id in (select top 1 id from Customer_checknumber_setting where customer_code = @customer_code order by id desc )";
                                dbAdapter.execNonQuery(cmd7);

                                //寫入log
                                remain = totalCount - usedCount;
                                cmd7.Parameters.AddWithValue("@pieces_count", "-1");
                                cmd7.Parameters.AddWithValue("@function_flag", "D1-1");
                                cmd7.Parameters.AddWithValue("@adjustment_reason", "API打單");
                                cmd7.Parameters.AddWithValue("@check_number", temp.checknumber);
                                cmd7.Parameters.AddWithValue("@remain_count", remain);
                                cmd7.CommandText = "insert into checknumber_record_for_bags_and_boxes (customer_code, pieces_count, remain_count, function_flag, update_date, adjustment_reason, check_number) values(@customer_code, @pieces_count, @remain_count, @function_flag, @update_date, @adjustment_reason, @check_number)";
                                dbAdapter.execNonQuery(cmd7);
                            }
                        }

                        cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);        //新增



                        #region 呼叫Sp
                        if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                        {

                            SqlCommand cmd2 = new SqlCommand();
                            cmd2.CommandText = "usp_GetLTShipFeeByRequestId";
                            cmd2.CommandType = CommandType.StoredProcedure;
                            cmd2.Parameters.AddWithValue("@request_id", request_id.ToString());
                            using (DataTable dt1 = dbAdapter.getDataTable(cmd2))
                            {
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    status_code = Convert.ToInt32(dt1.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                    if (status_code == 1)
                                    {
                                        supplier_fee = dt1.Rows[0]["supplier_fee"] != DBNull.Value ? Convert.ToInt32(dt1.Rows[0]["supplier_fee"]) : 0;       //配送費用

                                        //回寫到託運單的費用欄位
                                        using (SqlCommand cmd3 = new SqlCommand())
                                        {
                                            cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                            //cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                            cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                                            cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id.ToString());
                                            cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                                            try
                                            {
                                                dbAdapter.execNonQuery(cmd3);
                                            }
                                            catch (Exception ex)
                                            {
                                                //errortext += "更新託運單費用: " + ex.Message + "\n";
                                                System.Diagnostics.Debug.WriteLine(ex.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        if (string.IsNullOrEmpty(SpecialAreaAddressinfo.StationCode))
                        {
                            //寫入log
                            DataTable dt10 = new DataTable();
                            SqlCommand cmd10 = new SqlCommand();
                            cmd10.Parameters.AddWithValue("@customer_code", temp.customercode);
                            cmd10.Parameters.AddWithValue("@origin", "ApiService");
                            cmd10.CommandText = "insert into APIorigin (customer_code,origin,cdate) values(@customer_code,@origin,GETDATE())";
                            dbAdapter.execNonQuery(cmd10);
                            error_msg = "完成託運單新增。";
                        }
                        else
                        {
                            error_msg = "完成託運單新增,特服區需額外加價。";
                        }

                        supplier_code = temp.suppliercode;
                        DateTime end_time = DateTime.Now;
                        writeLogForEDI("call", "EDIAPI", getJson, error_msg, temp.checknumber, temp.customercode, resultcode, start_time, end_time);     //log
                    }

                }
            }

            EndLabel:

            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, driver_name = driver_name, supplier_code = supplier_code, type = type, ShuttleStationCode = ShuttleStationCode }, Newtonsoft.Json.Formatting.Indented);

            return strAppRequest;
        }
        #endregion


        #region getToken
        /// <summary>
        /// 建立Token
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "建立Token", EnableSession = true)]
        public void getToken(string Accoutno, string psw)
        {
            System.Collections.Hashtable Res = new System.Collections.Hashtable();
            //產生 Token
            string key = "B69CCA65B4D0B705E37080676E5F5DBBF4EA0E7A0EBD7C60CCE2024E218A53BB";
            DataTable dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand())
            {
                MD5 md5 = MD5.Create();
                string result = GetMd5Hash(md5, psw);
                cmd.Parameters.AddWithValue("@account_code", Accoutno);
                cmd.Parameters.AddWithValue("@password", result);
                cmd.CommandText = @"select * from tbAccounts where account_code = @account_code and (password=@password  or @password='5ac5cf299068ff4d958a311c56a4b3cb')";
                dt = dbAdapter.getDataTable(cmd);
            }
            if (dt.Rows.Count <= 0)
            {
                //return new Token
                //{
                //    //Token 為 iv + encrypt + signature，並用 . 串聯
                //    access_token = "帳號密碼輸入錯誤，無法進行認證。",
                //    //Refresh Token 使用 Guid 產生
                //    refresh_token ="",
                //    expires_in = 0,
                //};
                //return "帳號密碼輸入錯誤，無法進行認證。";

                Res.Add("error_msg", "帳號密碼輸入錯誤，無法進行認證。");
                DoContextResponse(DoHashTableToString(Res));
            }
            else
            {
                User user = new User();
                user.account_code = Accoutno;
                //產生 Token
                var exp = 3600;   //過期時間(秒)

                //稍微修改 Payload 將使用者資訊和過期時間分開
                var payload = new Payload
                {
                    info = user,
                    //Unix 時間戳
                    exp = Convert.ToInt32(
                        (DateTime.Now.AddSeconds(exp) -
                         new DateTime(1970, 1, 1)).TotalSeconds)
                };

                var json = JsonConvert.SerializeObject(payload);
                var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
                var iv = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);

                //使用 AES 加密 Payload
                var encrypt = AESEncrypt(base64, key.Substring(0, 16), iv);

                //取得簽章
                //var signature = ComputeHMACSHA256(iv + "." + encrypt, key.Substring(0, 64));

                //return new Token
                //{
                //    //Token 為 iv + encrypt + signature，並用 . 串聯
                //    access_token = iv + "." + encrypt + "." + signature,
                //    //Refresh Token 使用 Guid 產生
                //    refresh_token = Guid.NewGuid().ToString().Replace("-", ""),
                //    expires_in = exp,
                //};
                Res.Add("access_token", base64);
                DoContextResponse(DoHashTableToString(Res));

            }

            //return base64;
        }
        #endregion

        /// <summary>
        /// 蝦皮銷單使用
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "蝦皮取消訂單", EnableSession = true)]
        public string CheckNumbersWriteOff(string jsonChecknumbers)
        {
            writeLog("call", "CheckNumbersWriteOff", jsonChecknumbers, "");
            Boolean resultcode = true;
            string error_msg = string.Empty;

            try
            {

                List<Shopee> requests = JsonConvert.DeserializeObject<List<Shopee>>(jsonChecknumbers);
                List<string> checknumbers = requests.Select(a => a.checknumber).ToList();

                //using (SqlCommand cmd = new SqlCommand())
                //{
                //    DataTable dt = new DataTable();
                //    cmd.Parameters.AddWithValue("@check_number", checknumbers);
                //    cmd.CommandText = "select customer_code,latest_scan_item from tcDeliveryRequests where check_number = @check_number";
                //    dt = dbAdapter.getDataTable(cmd);
                //}
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    sss = dt.Rows[i]["scan_name"].ToString();
                //    aaa = dt.Rows[i]["scan_date"].ToString();
                //}

                DataTable dt2 = new DataTable();
                using (SqlCommand cmd = new SqlCommand())
                {
                    for (int i = 0; i < requests.Count; i++)
                    {
                        cmd.Parameters.Clear();
                        #region 關鍵字
                        cmd.Parameters.AddWithValue("@check_number", checknumbers[i]);
                        #endregion
                        cmd.CommandText = string.Format(@"select S.receive_option,　cancel_date, request_id, customer_code,latest_scan_item from tcDeliveryRequests R left join ttDeliveryScanLog S with(nolock) on s.check_number = r.check_number　and r.latest_scan_date = S.scan_date where R.check_number = @check_number");

                        dt2 = dbAdapter.getDataTable(cmd);
                    }
                }
                string customer_code = "";
                string latest_scan_item = "";
                string request_id = "";
                string cancel_date = "";
                string receive_option = "";

                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    Request_Status_Detail rsd = new Request_Status_Detail();
                    customer_code = dt2.Rows[i]["customer_code"].ToString();
                    latest_scan_item = dt2.Rows[i]["latest_scan_item"].ToString();
                    request_id = dt2.Rows[i]["request_id"].ToString();
                    cancel_date = dt2.Rows[i]["cancel_date"].ToString();
                    receive_option = dt2.Rows[i]["receive_option"].ToString();
                }
                if (requests.Count <= 0)
                {
                    resultcode = false;
                    error_msg = "請輸入貨號";
                }
                else if (customer_code != "F1300600002")
                {
                    resultcode = false;
                    error_msg = "非蝦皮客代";
                }
                else if (cancel_date != "")
                {
                    resultcode = false;
                    error_msg = "此筆已經銷單";
                }
                else if (latest_scan_item == "5" & receive_option == "20")
                {
                    resultcode = false;
                    error_msg = "此單已有貨態歷程，若有銷單需求，請洽詢全速配業務單位。";
                }
                else if (latest_scan_item == "6" || latest_scan_item == "7" || latest_scan_item == "1" || latest_scan_item == "2" || latest_scan_item == "3" || latest_scan_item == "4")
                {
                    resultcode = false;
                    error_msg = "此單已有貨態歷程，若有銷單需求，請洽詢全速配業務單位。";
                }
                else
                {
                    for (int i = 0; i < requests.Count; i++)
                    {
                        using (SqlCommand sql = new SqlCommand())
                        {
                            DataTable dataTable = new DataTable();
                            sql.Parameters.AddWithValue("@check_number", checknumbers[i]);
                            sql.Parameters.AddWithValue("@customer_code", customer_code);
                            sql.Parameters.AddWithValue("@request_id", request_id);
                            sql.CommandText = "update tcDeliveryRequests set cancel_date = GETDATE() where check_number = @check_number";
                            dbAdapter.execNonQuery(sql);
                            sql.CommandText = "insert into ttDeliveryRequestsRecord ( create_date, modify_date, request_id, create_user, delete_user, record_memo, check_number, customer_code, delete_date) values ( GETDATE(),GETDATE(),@request_id, 'skyeyes', 'skyeyes', '蝦皮API銷單', @check_number, @customer_code, GETDATE())";
                            dbAdapter.execNonQuery(sql);

                        }
                    }
                    error_msg = "銷單完成";
                }
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
            }
            string strAppRequest = JsonConvert.SerializeObject(new Request_Msg { isSuccess = resultcode, msg = error_msg }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;
        }



        /// <summary>
        /// 依託運單號查詢貨態回傳
        /// </summary>
        /// <param name="scancode">託運單號</param>
        /// <param name="type">0:取得最新狀態(預設)，1:取得歷程狀態</param>
        /// <returns></returns>
        [WebMethod(Description = "依託運單號查詢貨態回傳", EnableSession = true)]
        public string GetAttendanceType(string scancode, string type)
        {

            writeLog("call", "GetAttendanceType", "GetAttendanceType", scancode);
            string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataTable dtAppRequest = new DataTable();
            Boolean resultcode = true;
            string error_msg = string.Empty;
            string driver_name = string.Empty;
            string supplier_code = string.Empty;
            List<Request_Status_Detail> l_rsd = new List<Request_Status_Detail>();
            if (scancode.Equals(""))
            {
                resultcode = false;
                error_msg = "請輸入託運單號!!";
            }
            else
            {
                if (!scancode.Equals(""))
                {
                    Regex reg = new Regex(";");
                    if (reg.IsMatch(scancode))
                    {
                        //多筆
                        string[] l_array = scancode.Split(';');
                        foreach (string l_check_number in l_array)//託運單號
                        {
                            DataTable dt2 = new DataTable();
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                if (type == "" || type == "0")
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    #endregion
                                    cmd.CommandText = string.Format(@"
                                               Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'   END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc
                                                ");
                                }
                                else
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    #endregion
                                    cmd.CommandText = string.Format(@"
                                              Select top(1)  ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'   END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc
                                                ");
                                }
                                dt2 = dbAdapter.getDataTable(cmd);
                            }
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                Request_Status_Detail rsd = new Request_Status_Detail();
                                rsd.Type = dt2.Rows[i]["scan_name"].ToString();
                                rsd.Time = dt2.Rows[i]["scan_date"].ToString();
                                rsd.Salesrep_Code = dt2.Rows[i]["driver_code"].ToString();
                                rsd.Salesrep_Phone = "";
                                rsd.Question_type = dt2.Rows[i]["exception_option"].ToString();
                                rsd.Question_Memo = dt2.Rows[i]["status"].ToString();
                                l_rsd.Add(rsd);
                            }

                        }
                    }
                    else
                    {
                        //單筆
                        DataTable dt2 = new DataTable();

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            if (type == "" || type == "0")
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字
                                cmd.Parameters.AddWithValue("@check_number", scancode);
                                #endregion
                                cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'   END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc

                                                ");
                            }
                            else
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字
                                cmd.Parameters.AddWithValue("@check_number", scancode);
                                #endregion
                                cmd.CommandText = string.Format(@"
                                                Select top(1) ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'  END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc
                                                ");
                            }
                            dt2 = dbAdapter.getDataTable(cmd);
                        }
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            Request_Status_Detail rsd = new Request_Status_Detail();
                            rsd.Type = dt2.Rows[i]["scan_name"].ToString();
                            rsd.Time = dt2.Rows[i]["scan_date"].ToString();
                            rsd.Salesrep_Code = dt2.Rows[i]["driver_code"].ToString();
                            rsd.Salesrep_Phone = "";
                            rsd.Question_type = dt2.Rows[i]["exception_option"].ToString();
                            rsd.Question_Memo = dt2.Rows[i]["status"].ToString();
                            l_rsd.Add(rsd);
                        }
                    }
                }

            }
            object[] a = l_rsd.ToArray();
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = JsonConvert.SerializeObject(new trueJson { isSuccess = resultcode, Request_Status_Detail = a }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;

        }

        /// <summary>
        /// 依託運單號查詢貨態回傳
        /// </summary>
        /// <param name="scancode">託運單號</param>
        /// <param name="type">0:取得最新狀態(預設)，1:取得歷程狀態</param>
        /// <returns></returns>
        [WebMethod(Description = "查詢單筆貨件貨態詳述", EnableSession = true)]
        public string GetTrackingInformation(string scancode, string type)
        {

            writeLog("call", "GetTrackingInformation", "GetTrackingInformation", "");
            string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataTable dtAppRequest = new DataTable();
            Boolean resultcode = true;
            string error_msg = string.Empty;
            string driver_name = string.Empty;
            string supplier_code = string.Empty;
            List<Request_Status_Detail> l_rsd = new List<Request_Status_Detail>();
            if (scancode.Equals(""))
            {
                resultcode = false;
                error_msg = "請輸入託運單號!!";
            }
            else
            {
                if (!scancode.Equals(""))
                {
                    Regex reg = new Regex(";");
                    if (reg.IsMatch(scancode))
                    {
                        //多筆
                        string[] l_array = scancode.Split(';');
                        foreach (string l_check_number in l_array)//託運單號
                        {
                            DataTable dt2 = new DataTable();
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                if (type == "" || type == "0")
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    #endregion
                                    cmd.CommandText = string.Format(@"
                                               Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'  when '7' then '發送' when '6' then '卸集'   END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 --CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                case  when A.scan_item ='5'and (A.receive_option = '20' or A.receive_option is null) then '取件成功，貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tt.pieces as varchar(10))+'件。'
                                                      when A.scan_item ='5'and A.receive_option = '16'  then '與寄件人另約時間取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '6'   then '未能聯繫寄件人，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '18'  then '無貨可取，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '15'  then '司機取件中。'
	                                                  when A.scan_item ='5'and A.receive_option = '8'   then '包裹超過規格，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '16'  then '約定再取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '4'   then '包裹已被同業收走，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '22'  then '公司休息，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='6'                             then '貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tc.pieces as varchar(10))+'件。'
	                                                  when A.scan_item ='7'                              then '貨件已由' +  tb.station_name + '所發出。'
	                                                  when A.scan_item ='1'                              then '貨件已抵達' + td.station_name + '所。'
	                                                  when A.scan_item ='2'                              then '貨件由' + td.station_name + '所人員配送中'
                                                      when A.scan_item ='3'and A.arrive_option = '1'     then '送達客戶不在，' + td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '2'     then '另約時間配送，目前由' +  td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '3'     then '貨件已由' + td.station_name + '所送達，貨物件數共' +CAST(tc.pieces as varchar(10))+ '件。'
	                                                  when A.scan_item ='3'and A.arrive_option = '4'     then '收貨人拒絕收件，'+ td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '5'     then '收貨人地址異常，' + td.station_name + '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '6'    then  '該地址查無此人，' + td.station_name +  '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '7'    then '缺件配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '8'    then '破損配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '9'    then '短少配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '10'   then '原單退回。'
                                                 end as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 left join tcDeliveryRequests tt with(nolock)on tt.check_number = A.check_number 
                                                 left join tbStation tb with(nolock)on tt.supplier_code = tb.station_code
                                                 left join tcDeliveryRequests tc with(nolock)on tc.check_number = A.check_number 
                                                 left join tbStation td with(nolock)on tc.area_arrive_code = td.station_scode
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc
                                                ");
                                }
                                else
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    #endregion
                                    cmd.CommandText = string.Format(@"
                                              Select top(1)  ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'  when '7' then '發送' when '6' then '卸集'  END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 --CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                case  when A.scan_item ='5'and (A.receive_option = '20' or A.receive_option is null) then '取件成功，貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tt.pieces as varchar(10))+'件。'
                                                      when A.scan_item ='5'and A.receive_option = '16'  then '與寄件人另約時間取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '6'   then '未能聯繫寄件人，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '18'  then '無貨可取，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '15'  then '司機取件中。'
	                                                  when A.scan_item ='5'and A.receive_option = '8'   then '包裹超過規格，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '16'  then '約定再取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '4'   then '包裹已被同業收走，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '22'  then '公司休息，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='6'                             then '貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tc.pieces as varchar(10))+'件。'
	                                                  when A.scan_item ='7'                              then '貨件已由' +  tb.station_name + '所發出。'
	                                                  when A.scan_item ='1'                              then '貨件已抵達' + td.station_name + '所。'
	                                                  when A.scan_item ='2'                              then '貨件由' + td.station_name + '所人員配送中'
                                                      when A.scan_item ='3'and A.arrive_option = '1'     then '送達客戶不在，' + td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '2'     then '另約時間配送，目前由' +  td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '3'     then '貨件已由' + td.station_name + '所送達，貨物件數共' +CAST(tc.pieces as varchar(10))+ '件。'
	                                                  when A.scan_item ='3'and A.arrive_option = '4'     then '收貨人拒絕收件，'+ td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '5'     then '收貨人地址異常，' + td.station_name + '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '6'    then  '該地址查無此人，' + td.station_name +  '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '7'    then '缺件配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '8'    then '破損配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '9'    then '短少配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '10'   then '原單退回。'
                                                 end as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 left join tcDeliveryRequests tt with(nolock)on tt.check_number = A.check_number 
                                                 left join tbStation tb with(nolock)on tt.supplier_code = tb.station_code
                                                 left join tcDeliveryRequests tc with(nolock)on tc.check_number = A.check_number 
                                                 left join tbStation td with(nolock)on tc.area_arrive_code = td.station_scode
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc
                                                ");
                                }
                                dt2 = dbAdapter.getDataTable(cmd);
                            }
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                Request_Status_Detail rsd = new Request_Status_Detail();
                                rsd.Type = dt2.Rows[i]["scan_name"].ToString();
                                rsd.Time = dt2.Rows[i]["scan_date"].ToString();
                                rsd.Salesrep_Code = dt2.Rows[i]["driver_code"].ToString();
                                rsd.Salesrep_Phone = "";
                                rsd.Question_type = dt2.Rows[i]["exception_option"].ToString();
                                rsd.Question_Memo = dt2.Rows[i]["status"].ToString();
                                l_rsd.Add(rsd);
                            }

                        }
                    }
                    else
                    {
                        //單筆
                        DataTable dt2 = new DataTable();

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            if (type == "" || type == "0")
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字
                                cmd.Parameters.AddWithValue("@check_number", scancode);
                                #endregion
                                cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'   when '7' then '發送' when '6' then '卸集' END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 --CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                 case  when A.scan_item ='5'and (A.receive_option = '20' or A.receive_option is null) then '取件成功，貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tt.pieces as varchar(10))+'件。'
                                                      when A.scan_item ='5'and A.receive_option = '16'  then '與寄件人另約時間取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '6'   then '未能聯繫寄件人，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '18'  then '無貨可取，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '15'  then '司機取件中。'
	                                                  when A.scan_item ='5'and A.receive_option = '8'   then '包裹超過規格，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '16'  then '約定再取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '4'   then '包裹已被同業收走，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '22'  then '公司休息，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='6'                             then '貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tc.pieces as varchar(10))+'件。'
	                                                  when A.scan_item ='7'                              then '貨件已由' +  tb.station_name + '所發出。'
	                                                  when A.scan_item ='1'                              then '貨件已抵達' + td.station_name + '所。'
	                                                  when A.scan_item ='2'                              then '貨件由' + td.station_name + '所人員配送中'
                                                      when A.scan_item ='3'and A.arrive_option = '1'     then '送達客戶不在，' + td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '2'     then '另約時間配送，目前由' +  td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '3'     then '貨件已由' + td.station_name + '所送達，貨物件數共' +CAST(tc.pieces as varchar(10))+ '件。'
	                                                  when A.scan_item ='3'and A.arrive_option = '4'     then '收貨人拒絕收件，'+ td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '5'     then '收貨人地址異常，' + td.station_name + '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '6'    then  '該地址查無此人，' + td.station_name +  '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '7'    then '缺件配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '8'    then '破損配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '9'    then '短少配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '10'   then '原單退回。'
                                                 end as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 left join tcDeliveryRequests tt with(nolock)on tt.check_number = A.check_number 
                                                 left join tbStation tb with(nolock)on tt.supplier_code = tb.station_code
                                                 left join tcDeliveryRequests tc with(nolock)on tc.check_number = A.check_number 
                                                 left join tbStation td with(nolock)on tc.area_arrive_code = td.station_scode
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc

                                                ");
                            }
                            else
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字
                                cmd.Parameters.AddWithValue("@check_number", scancode);
                                #endregion
                                cmd.CommandText = string.Format(@"
                                                Select top(1) ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達' WHEN '4' THEN '簽單' WHEN '5' THEN '集貨'  when '7' then '發送' when '6' then '卸集' END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 --CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                 case  when A.scan_item ='5'and (A.receive_option = '20' or A.receive_option is null) then '取件成功，貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tt.pieces as varchar(10))+'件。'
                                                      when A.scan_item ='5'and A.receive_option = '16'  then '與寄件人另約時間取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '6'   then '未能聯繫寄件人，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '18'  then '無貨可取，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '15'  then '司機取件中。'
	                                                  when A.scan_item ='5'and A.receive_option = '8'   then '包裹超過規格，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '16'  then '約定再取件，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '4'   then '包裹已被同業收走，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '22'  then '公司休息，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='5'and A.receive_option = '12'  then '地址錯誤，取件尚未成功。'
	                                                  when A.scan_item ='6'                             then '貨件已抵達' + tb.station_name + '所，貨件整理中，貨物件數共'+CAST(tc.pieces as varchar(10))+'件。'
	                                                  when A.scan_item ='7'                              then '貨件已由' +  tb.station_name + '所發出。'
	                                                  when A.scan_item ='1'                              then '貨件已抵達' + td.station_name + '所。'
	                                                  when A.scan_item ='2'                              then '貨件由' + td.station_name + '所人員配送中'
                                                      when A.scan_item ='3'and A.arrive_option = '1'     then '送達客戶不在，' + td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '2'     then '另約時間配送，目前由' +  td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '3'     then '貨件已由' + td.station_name + '所送達，貨物件數共' +CAST(tc.pieces as varchar(10))+ '件。'
	                                                  when A.scan_item ='3'and A.arrive_option = '4'     then '收貨人拒絕收件，'+ td.station_name + '所保管中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '5'     then '收貨人地址異常，' + td.station_name + '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '6'    then  '該地址查無此人，' + td.station_name +  '所處理中。'
	                                                  when A.scan_item ='3'and A.arrive_option = '7'    then '缺件配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '8'    then '破損配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '9'    then '短少配交。'
	                                                  when A.scan_item ='3'and A.arrive_option = '10'   then '原單退回。'
                                                 end as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 left join tcDeliveryRequests tt with(nolock)on tt.check_number = A.check_number 
                                                 left join tbStation tb with(nolock)on tt.supplier_code = tb.station_code
                                                 left join tcDeliveryRequests tc with(nolock)on tc.check_number = A.check_number 
                                                 left join tbStation td with(nolock)on tc.area_arrive_code = td.station_scode
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc
                                                ");
                            }
                            dt2 = dbAdapter.getDataTable(cmd);
                        }
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            Request_Status_Detail rsd = new Request_Status_Detail();
                            rsd.Type = dt2.Rows[i]["scan_name"].ToString();
                            rsd.Time = dt2.Rows[i]["scan_date"].ToString();
                            rsd.Salesrep_Code = dt2.Rows[i]["driver_code"].ToString();
                            rsd.Salesrep_Phone = "";
                            rsd.Question_type = dt2.Rows[i]["exception_option"].ToString();
                            rsd.Question_Memo = dt2.Rows[i]["status"].ToString();
                            l_rsd.Add(rsd);
                        }
                    }
                }

            }
            object[] a = l_rsd.ToArray();
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = JsonConvert.SerializeObject(new trueJson { isSuccess = resultcode, Request_Status_Detail = a }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;

        }

        /// <summary>
        /// 依託運單號查詢貨態回傳(MOMO)
        /// </summary>
        /// <param name="scancode">託運單號</param>
        /// <param name="type">0:取得最新狀態(預設)，1:取得歷程狀態</param>
        /// <returns></returns>
        [WebMethod(Description = "依託運單號查詢貨態回傳(MOMO)", EnableSession = true)]
        public string GetMOMOType(string scancode, string type)
        {

            writeLog("call", "GetMOMOType", "GetMOMOType", "");
            string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataTable dtAppRequest = new DataTable();
            Boolean resultcode = true;
            string error_msg = string.Empty;
            string driver_name = string.Empty;
            string supplier_code = string.Empty;
            List<Request_MOMO_Detail> l_rsd = new List<Request_MOMO_Detail>();
            if (scancode.Equals(""))
            {
                resultcode = false;
                error_msg = "請輸入託運單號!!";
            }
            else
            {
                if (!scancode.Equals(""))
                {
                    Regex reg = new Regex(";");
                    if (reg.IsMatch(scancode))
                    {
                        string wherestr1 = string.Empty;
                        string wherestr2 = string.Empty;
                        //多筆
                        string[] l_array = scancode.Split(';');
                        foreach (string l_check_number in l_array)//託運單號
                        {
                            DataTable dt2 = new DataTable();
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                if (type == "" || type == "0")
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                    cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                    #endregion
                                    cmd.CommandText = string.Format(@"
DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 
                                               SET @statement ='
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name,S.station_code, C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode {0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2);
                                }
                                else
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                    cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                    #endregion
                                    cmd.CommandText = string.Format(@"
DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 
                                              SET @statement ='
                                                Select top 1 ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name,S.station_code, C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode {0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2
                                                );
                                }
                                dt2 = dbAdapter.getDataTable(cmd);
                            }
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                Request_MOMO_Detail rsd = new Request_MOMO_Detail();
                                rsd.NO = dt2.Rows[i]["NO"].ToString();
                                rsd.scan_date = dt2.Rows[i]["scan_date"].ToString();
                                rsd.scan_name = dt2.Rows[i]["scan_name"].ToString();
                                rsd.supplier_name = dt2.Rows[i]["station_name"].ToString();
                                rsd.station_code = dt2.Rows[i]["station_code"].ToString();
                                rsd.station_name = dt2.Rows[i]["station_Name"].ToString();
                                //rsd.ItemCodes = dt2.Rows[i]["arrive_option"].ToString();
                                rsd.status = dt2.Rows[i]["status"].ToString();
                                rsd.driver_code = dt2.Rows[i]["driver_code"].ToString();
                                rsd.driver_name = dt2.Rows[i]["driver_name"].ToString();
                                l_rsd.Add(rsd);
                            }

                        }
                    }
                    else
                    {
                        //單筆
                        DataTable dt2 = new DataTable();
                        string wherestr1 = string.Empty;
                        string wherestr2 = string.Empty;
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            if (type == "" || type == "0")
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字



                                if (scancode != "")
                                {
                                    cmd.Parameters.AddWithValue("@check_number", scancode);
                                }
                                cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                #endregion
                                cmd.CommandText = string.Format(@"
                                                DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 
                                                SET @statement ='
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name,S.station_code, C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode

{0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2);
                            }
                            else
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字
                                cmd.Parameters.AddWithValue("@check_number", scancode);
                                cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                #endregion
                                cmd.CommandText = string.Format(@" DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 
                                                SET @statement ='
                                                Select top 1 ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name,S.station_code, C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode {0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2);
                            }
                            dt2 = dbAdapter.getDataTable(cmd);
                        }
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            Request_MOMO_Detail rsd = new Request_MOMO_Detail();
                            rsd.NO = dt2.Rows[i]["NO"].ToString();
                            rsd.scan_date = dt2.Rows[i]["scan_date"].ToString();
                            rsd.scan_name = dt2.Rows[i]["scan_name"].ToString();
                            rsd.supplier_name = dt2.Rows[i]["supplier_name"].ToString();
                            rsd.station_code = dt2.Rows[i]["station_code"].ToString();
                            rsd.station_name = dt2.Rows[i]["station_Name"].ToString();
                            //rsd.ItemCodes = dt2.Rows[i]["arrive_option"].ToString();
                            rsd.status = dt2.Rows[i]["status"].ToString();
                            rsd.driver_code = dt2.Rows[i]["driver_code"].ToString();
                            rsd.driver_name = dt2.Rows[i]["driver_name"].ToString();
                            l_rsd.Add(rsd);
                        }
                    }
                }

            }
            object[] a = l_rsd.ToArray();
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = JsonConvert.SerializeObject(new trueJson { isSuccess = resultcode, Request_Status_Detail = a }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;

        }

        /// <summary>
        /// 依託運單號查詢貨態回傳
        /// </summary>
        /// <param name="scancode">託運單號</param>
        /// <param name="type">0:取得最新狀態(預設)，1:取得歷程狀態</param>
        /// <returns></returns>
        [WebMethod(Description = "依託運單號查詢貨態回傳", EnableSession = true)]
        public string GetTraceType(string scancode, string type)
        {
            writeLog("call", "GetTraceType", "GetTraceType", "");
            string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataTable dtAppRequest = new DataTable();
            Boolean resultcode = true;
            string error_msg = string.Empty;
            string driver_name = string.Empty;
            string supplier_code = string.Empty;
            List<Request_MOMO_Detail> l_rsd = new List<Request_MOMO_Detail>();
            if (scancode.Equals(""))
            {
                resultcode = false;
                error_msg = "請輸入託運單號!!";
            }
            else
            {
                if (!scancode.Equals(""))
                {
                    Regex reg = new Regex(";");
                    if (reg.IsMatch(scancode))
                    {
                        string wherestr1 = string.Empty;
                        string wherestr2 = string.Empty;
                        //多筆
                        string[] l_array = scancode.Split(';');
                        foreach (string l_check_number in l_array)//託運單號
                        {
                            DataTable dt2 = new DataTable();
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                if (type == "" || type == "0")
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                    //cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                    #endregion
                                    cmd.CommandText = string.Format(@"

DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 
                                               SET @statement ='
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name,S.station_code, C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode {0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2);
                                }
                                else
                                {
                                    cmd.Parameters.Clear();
                                    #region 關鍵字
                                    cmd.Parameters.AddWithValue("@check_number", l_check_number);
                                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                    //cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                    #endregion
                                    cmd.CommandText = string.Format(@"

DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 
                                               SET @statement ='
                                                Select top 1 ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name, S.station_code,C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode {0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2
                                                );
                                }
                                dt2 = dbAdapter.getDataTable(cmd);
                            }
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                Request_MOMO_Detail rsd = new Request_MOMO_Detail();
                                rsd.NO = dt2.Rows[i]["NO"].ToString();
                                rsd.scan_date = dt2.Rows[i]["scan_date"].ToString();
                                rsd.scan_name = dt2.Rows[i]["scan_name"].ToString();
                                rsd.supplier_name = dt2.Rows[i]["station_name"].ToString();
                                rsd.station_code = dt2.Rows[i]["station_code"].ToString();
                                rsd.station_name = dt2.Rows[i]["station_Name"].ToString();
                                //rsd.ItemCodes = dt2.Rows[i]["arrive_option"].ToString();
                                rsd.status = dt2.Rows[i]["status"].ToString();
                                rsd.driver_code = dt2.Rows[i]["driver_code"].ToString();
                                rsd.driver_name = dt2.Rows[i]["driver_name"].ToString();
                                l_rsd.Add(rsd);
                            }

                        }
                    }
                    else
                    {
                        //單筆
                        DataTable dt2 = new DataTable();
                        string wherestr1 = string.Empty;
                        string wherestr2 = string.Empty;
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            if (type == "" || type == "0")
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字



                                if (scancode != "")
                                {
                                    cmd.Parameters.AddWithValue("@check_number", scancode);
                                }
                                cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                #endregion
                                cmd.CommandText = string.Format(@"
DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 

                                               SET @statement ='
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name,S.station_code, C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode {0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2);
                            }
                            else
                            {
                                cmd.Parameters.Clear();
                                #region 關鍵字
                                cmd.Parameters.AddWithValue("@check_number", scancode);
                                cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                                cmd.Parameters.AddWithValue("@DeliveryType", "D");
                                #endregion
                                cmd.CommandText = string.Format(@" DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                 
                                                SET @statement ='
                                                Select top 1 ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item, A.area, CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse,
case when A.check_number like ''990%'' then R.send_contact 
when (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) <>'''' then
 (select top 1 check_number from tcDeliveryRequests where send_contact = '''+ @check_number +''' and DeliveryType = ''R'' and Less_than_truckload = 1) 
else
A.tracking_number end tracking_number,
S.station_name,S.station_code, C.driver_mobile,
                                                Convert(varchar, A.scan_date,112)+''01'' as shift    
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate()




                                                left join tbStation S with(nolock) on C.station = S.station_scode {0}'
                                                
                                                  
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                SET @where = @where + 'and R.Less_than_truckload = '''+ @Less_than_truckload+ ''''



                                                {1}

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ", wherestr1, wherestr2);
                            }
                            dt2 = dbAdapter.getDataTable(cmd);
                        }
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            Request_MOMO_Detail rsd = new Request_MOMO_Detail();
                            rsd.NO = dt2.Rows[i]["NO"].ToString();
                            rsd.scan_date = dt2.Rows[i]["scan_date"].ToString();
                            rsd.scan_name = dt2.Rows[i]["scan_name"].ToString();
                            rsd.supplier_name = dt2.Rows[i]["supplier_name"].ToString();
                            rsd.station_code = dt2.Rows[i]["station_code"].ToString();
                            rsd.station_name = dt2.Rows[i]["station_Name"].ToString();
                            //rsd.ItemCodes = dt2.Rows[i]["arrive_option"].ToString();
                            rsd.status = dt2.Rows[i]["status"].ToString();
                            rsd.driver_code = dt2.Rows[i]["driver_code"].ToString();
                            rsd.driver_name = dt2.Rows[i]["driver_name"].ToString();
                            l_rsd.Add(rsd);
                        }
                    }
                }

            }
            object[] a = l_rsd.ToArray();
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = JsonConvert.SerializeObject(new trueJson { isSuccess = resultcode, Request_Status_Detail = a }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;

        }

        /// <summary>
        /// 依出貨日期、查詢全數配、宅配通訂單數
        /// </summary>
        /// <param name="print_date">出貨日期</param>
        /// <param name="type">0:全部、1:全數配、2:宅配通</param>
        /// <returns></returns>
        [WebMethod(Description = "依出貨日期、查詢全數配、宅配通訂單數", EnableSession = true)]
        public string GetTotalCount(string print_date, string type)
        {
            writeLog("call", "GetTotalCount", "GetTotalCount", "");
            DataTable dtAppRequest = new DataTable();
            Boolean resultcode = true;
            string error_msg = string.Empty;
            string driver_name = string.Empty;
            string supplier_code = string.Empty;
            List<Request_Detail> l_rsd = new List<Request_Detail>();
            DataTable totalcountdt = new DataTable();
            if (print_date.Equals(""))
            {
                resultcode = false;
                error_msg = "請輸入查詢出貨日期!!";
            }
            else
            {
                DataTable Detaildt = new DataTable();
                if (type == "" || type == "0")
                {
                    totalcountdt = new DataTable();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@print_date", print_date);
                        cmd.CommandText = @" select count(*) from tcDeliveryRequests where print_date >= convert(datetime,@print_date) and print_date < CONVERT (datetime,DATEADD(DD,1,@print_date)) and pricing_type = '02' ";
                        totalcountdt = dbAdapter.getDataTable(cmd);
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@print_date", print_date);
                        cmd.CommandText = @"select 
                                              check_number ,
                                              [pricing_type],
                                              order_number,
                                              receive_city+receive_area+receive_address as receiveaddress,
                                              area_arrive_code,send_city+send_area+send_address as sendaddress,
                                              case Distributor when '01' then '全速配'
                                               when '02' then '宅配通'
                                                when '全速配' then '全速配'
	                                              when '宅配通' then '宅配通'
	                                              end Distributor
                                              from tcDeliveryRequests
                                              where 
                                              print_date >= convert(datetime,@print_date) and print_date < CONVERT (datetime,DATEADD(DD,1,@print_date))
                                              and [pricing_type] = '02'";
                        Detaildt = dbAdapter.getDataTable(cmd);
                    }

                    for (int i = 0; i < Detaildt.Rows.Count; i++)
                    {
                        Request_Detail rsd = new Request_Detail();
                        rsd.check_number = Detaildt.Rows[i]["check_number"].ToString();
                        rsd.order_number = Detaildt.Rows[i]["order_number"].ToString();
                        rsd.pricing_type = Detaildt.Rows[i]["pricing_type"].ToString();
                        rsd.receiveaddress = Detaildt.Rows[i]["receiveaddress"].ToString();
                        rsd.sendaddress = Detaildt.Rows[0]["sendaddress"].ToString();
                        rsd.Distributor = Detaildt.Rows[i]["Distributor"].ToString();
                        l_rsd.Add(rsd);
                    }
                }
                else if (type == "1")
                {
                    totalcountdt = new DataTable();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@print_date", print_date);
                        cmd.CommandText = @" select count(*) from tcDeliveryRequests where print_date >= convert(datetime,@print_date) and print_date < CONVERT (datetime,DATEADD(DD,1,@print_date)) 
                                             and pricing_type = '02' and (Distributor = '01' or Distributor = '全速配')";
                        totalcountdt = dbAdapter.getDataTable(cmd);
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@print_date", print_date);
                        cmd.CommandText = @"select 
                                              check_number ,
                                              [pricing_type],
                                              order_number,
                                              receive_city+receive_area+receive_address as receiveaddress,
                                              area_arrive_code,send_city+send_area+send_address as sendaddress,
                                              case Distributor when '01' then '全速配'
                                               when '02' then '宅配通'
                                                when '全速配' then '全速配'
	                                              when '宅配通' then '宅配通'
	                                              end Distributor
                                              from tcDeliveryRequests
                                              where 
                                              print_date >= convert(datetime,@print_date) and print_date < CONVERT (datetime,DATEADD(DD,1,@print_date))
                                              and [pricing_type] = '02' and (Distributor = '01' or Distributor = '全速配')";
                        Detaildt = dbAdapter.getDataTable(cmd);
                    }

                    for (int i = 0; i < Detaildt.Rows.Count; i++)
                    {
                        Request_Detail rsd = new Request_Detail();
                        rsd.check_number = Detaildt.Rows[i]["check_number"].ToString();
                        rsd.order_number = Detaildt.Rows[i]["order_number"].ToString();
                        rsd.pricing_type = Detaildt.Rows[i]["pricing_type"].ToString();
                        rsd.receiveaddress = Detaildt.Rows[i]["receiveaddress"].ToString();
                        rsd.sendaddress = Detaildt.Rows[0]["sendaddress"].ToString();
                        rsd.Distributor = Detaildt.Rows[i]["Distributor"].ToString();
                        l_rsd.Add(rsd);
                    }
                }
                else if (type == "2")
                {
                    totalcountdt = new DataTable();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@print_date", print_date);
                        cmd.CommandText = @" select count(*) from tcDeliveryRequests where print_date >= convert(datetime,@print_date) and print_date < CONVERT (datetime,DATEADD(DD,1,@print_date)) 
                                             and pricing_type = '02' and (Distributor = '02' or Distributor = '宅配通')";
                        totalcountdt = dbAdapter.getDataTable(cmd);
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@print_date", print_date);
                        cmd.CommandText = @"select 
                                              check_number ,
                                              [pricing_type],
                                              order_number,
                                              receive_city+receive_area+receive_address as receiveaddress,
                                              area_arrive_code,send_city+send_area+send_address as sendaddress,
                                              case Distributor when '01' then '全速配'
                                               when '02' then '宅配通'
                                                when '全速配' then '全速配'
	                                              when '宅配通' then '宅配通'
	                                              end Distributor
                                              from tcDeliveryRequests
                                              where 
                                              print_date >= convert(datetime,@print_date) and print_date < CONVERT (datetime,DATEADD(DD,1,@print_date))
                                              and [pricing_type] = '02' and (Distributor = '02' or Distributor = '宅配通')";
                        Detaildt = dbAdapter.getDataTable(cmd);
                    }

                    for (int i = 0; i < Detaildt.Rows.Count; i++)
                    {
                        Request_Detail rsd = new Request_Detail();
                        rsd.check_number = Detaildt.Rows[i]["check_number"].ToString();
                        rsd.order_number = Detaildt.Rows[i]["order_number"].ToString();
                        rsd.pricing_type = Detaildt.Rows[i]["pricing_type"].ToString();
                        rsd.receiveaddress = Detaildt.Rows[i]["receiveaddress"].ToString();
                        rsd.sendaddress = Detaildt.Rows[0]["sendaddress"].ToString();
                        rsd.Distributor = Detaildt.Rows[i]["Distributor"].ToString();
                        l_rsd.Add(rsd);
                    }
                }
            }
            int s = Convert.ToInt32(totalcountdt.Rows[0][0].ToString());
            object[] a = l_rsd.ToArray();
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = JsonConvert.SerializeObject(new trueJson { isSuccess = resultcode, totalcountdt = s, Request_Status_Detail = a }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;
        }


        /// <summary>
        /// 依輸入地址回傳到著碼(棧板)
        /// </summary>
        /// <param name="City">縣/市</param>
        /// <param name="Area">區域</param>
        /// <returns></returns>
        [WebMethod(Description = "依輸入地址回傳到著碼(棧板)", EnableSession = true)]
        public string GetPalletNumber(string address)
        {
            writeLog("call", "GetPalletNumber", "GetPalletNumber", "");
            DataTable dtAppRequest = new DataTable();
            Boolean resultcode = true;
            string error_msg = string.Empty;
            string msg = string.Empty;

            //正規畫過後的縣/市
            string strCity = string.Empty;

            //正規畫過後的 區
            string strArea = string.Empty;


            if (address.Equals(""))
            {
                resultcode = false;
                msg = "999";
            }
            else
            {
                //先簡體轉繁體
                string ChinaFoTaiwan = ToTraditional(address);
                ChinaFoTaiwan = ChinaFoTaiwan.Replace(" ", "");
                ChinaFoTaiwan = ChinaFoTaiwan.Replace("臺灣省", "");

                //將轉為繁體的地址拆分縣市、區
                ADDRESS l_Address = new ADDRESS(ChinaFoTaiwan);

                DataTable Detaildt = new DataTable();

                using (SqlCommand cmd = new SqlCommand())
                {

                    #region 縣市正規畫 
                    //如果 縣/市 
                    switch (l_Address.City)
                    {   //符合         將 臺北市 塞入 strCity
                        case "台北市": strCity = "臺北市"; break;
                        //符合         將 台中市 塞入 strCity
                        case "台中市": strCity = "臺中市"; break;
                        //符合         將 台南市 塞入 strCity
                        case "台南市": strCity = "臺南市"; break;
                        //符合         將 桃園縣 塞入 strCity
                        case "桃園縣": strCity = "桃園市"; break;
                        //符合         將 高雄縣 塞入 strCity
                        case "高雄縣": strCity = "高雄市"; break;
                        //符合         將 新北市 塞入 strCity
                        case "台北縣": strCity = "新北市"; break;

                        default:
                            strCity = l_Address.City; break;
                    }
                    #endregion


                    #region 區 正規畫
                    /* 判斷法可以用 switch case (縣正規畫的那種)
                     * 也可以使用 if else 完全看個人習慣
                     * if(符合) {
                     *      程式碼....
                     * } else if(符合){
                     *      程式碼....
                     * }
                     */
                    if (strCity == "臺中市")
                    {
                        switch (l_Address.Region)
                        {
                            case "豐原市": strArea = "豐原區"; break;
                            case "后里鄉": strArea = "后里區"; break;
                            case "潭子鄉": strArea = "潭子區"; break;
                            case "大雅鄉": strArea = "大雅區"; break;
                            case "神岡鄉": strArea = "神岡區"; break;
                            case "東勢鎮": strArea = "東勢區"; break;
                            case "石岡鄉": strArea = "石岡區"; break;
                            case "新社鄉": strArea = "新社區"; break;
                            case "和平鄉": strArea = "和平區"; break;
                            case "清水鎮": strArea = "清水區"; break;
                            case "沙鹿鎮": strArea = "沙鹿區"; break;
                            case "梧棲鎮": strArea = "梧棲區"; break;
                            case "大甲鎮": strArea = "大甲區"; break;
                            case "大安鄉": strArea = "大安區"; break;
                            case "外埔鄉": strArea = "外埔區"; break;
                            case "龍井鄉": strArea = "龍井區"; break;
                            case "大肚鄉": strArea = "大肚區"; break;
                            case "大里市": strArea = "大里區"; break;
                            case "霧峰鄉": strArea = "霧峰區"; break;
                            case "太平市": strArea = "太平區"; break;
                            case "太平鄉": strArea = "太平區"; break;
                            case "烏日鄉": strArea = "烏日區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "臺南市")
                    {
                        switch (l_Address.Region)
                        {
                            case "中區": strArea = "中西區"; break;
                            case "西區": strArea = "中西區"; break;
                            case "新化鎮": strArea = "新化區"; break;
                            case "楠西鄉": strArea = "楠西區"; break;
                            case "南化鄉": strArea = "南化區"; break;
                            case "玉井鄉": strArea = "玉井區"; break;
                            case "左鎮鄉": strArea = "左鎮區"; break;
                            case "新市鄉": strArea = "新市區"; break;
                            case "山上鄉": strArea = "山上區"; break;
                            case "龍崎鄉": strArea = "龍崎區"; break;
                            case "永康鄉": strArea = "永康區"; break;
                            case "永康市": strArea = "永康區"; break;
                            case "歸仁鄉": strArea = "歸仁區"; break;
                            case "仁德鄉": strArea = "仁德區"; break;
                            case "關廟鄉": strArea = "關廟區"; break;
                            case "安定鄉": strArea = "安定區"; break;
                            case "善化鎮": strArea = "善化區"; break;
                            case "新營市": strArea = "新營區"; break;
                            case "鹽水鎮": strArea = "鹽水區"; break;
                            case "柳營鄉": strArea = "柳營區"; break;
                            case "白河鎮": strArea = "白河區"; break;
                            case "後壁鄉": strArea = "後壁區"; break;
                            case "東山鄉": strArea = "東山區"; break;
                            case "佳里鎮": strArea = "佳里區"; break;
                            case "西港鄉": strArea = "西港區"; break;
                            case "北門鄉": strArea = "北門區"; break;
                            case "學甲鎮": strArea = "學甲區"; break;
                            case "將軍鄉": strArea = "將軍區"; break;
                            case "麻豆鎮": strArea = "麻豆區"; break;
                            case "官田鄉": strArea = "官田區"; break;
                            case "下營鄉": strArea = "下營區"; break;
                            case "六甲鄉": strArea = "六甲區"; break;
                            case "大內鄉": strArea = "大內區"; break;
                            case "七股鄉": strArea = "七股區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "桃園市")
                    {
                        switch (l_Address.Region)
                        {
                            case "觀音鄉": strArea = "觀音區"; break;
                            case "觀音鎮": strArea = "觀音區"; break;
                            case "觀音市": strArea = "觀音區"; break;
                            case "觀音區": strArea = "觀音區"; break;
                            case "中壢鄉": strArea = "中壢區"; break;
                            case "中壢鎮": strArea = "中壢區"; break;
                            case "中壢市": strArea = "中壢區"; break;
                            case "中壢區": strArea = "中壢區"; break;
                            case "平鎮鄉": strArea = "平鎮區"; break;
                            case "平鎮鎮": strArea = "平鎮區"; break;
                            case "平鎮市": strArea = "平鎮區"; break;
                            case "平鎮區": strArea = "平鎮區"; break;
                            case "桃園鄉": strArea = "桃園區"; break;
                            case "桃園鎮": strArea = "桃園區"; break;
                            case "桃園市": strArea = "桃園區"; break;
                            case "桃園區": strArea = "桃園區"; break;
                            case "龜山鄉": strArea = "龜山區"; break;
                            case "龜山鎮": strArea = "龜山區"; break;
                            case "龜山市": strArea = "龜山區"; break;
                            case "龜山區": strArea = "龜山區"; break;
                            case "八德鄉": strArea = "八德區"; break;
                            case "八德鎮": strArea = "八德區"; break;
                            case "八德市": strArea = "八德區"; break;
                            case "八德區": strArea = "八德區"; break;
                            case "大溪鄉": strArea = "大溪區"; break;
                            case "大溪鎮": strArea = "大溪區"; break;
                            case "大溪市": strArea = "大溪區"; break;
                            case "大溪區": strArea = "大溪區"; break;
                            case "復興鄉": strArea = "復興區"; break;
                            case "復興鎮": strArea = "復興區"; break;
                            case "復興市": strArea = "復興區"; break;
                            case "復興區": strArea = "復興區"; break;
                            case "大園鄉": strArea = "大園區"; break;
                            case "大園鎮": strArea = "大園區"; break;
                            case "大園市": strArea = "大園區"; break;
                            case "大園區": strArea = "大園區"; break;
                            case "蘆竹鄉": strArea = "蘆竹區"; break;
                            case "蘆竹鎮": strArea = "蘆竹區"; break;
                            case "蘆竹市": strArea = "蘆竹區"; break;
                            case "蘆竹區": strArea = "蘆竹區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "高雄市")
                    {
                        switch (l_Address.Region)
                        {
                            case "鳳山市": strArea = "鳳山區"; break;
                            case "林園鄉": strArea = "林園區"; break;
                            case "大寮鄉": strArea = "大寮區"; break;
                            case "大樹鄉": strArea = "大樹區"; break;
                            case "大社鄉": strArea = "大社區"; break;
                            case "仁武鄉": strArea = "仁武區"; break;
                            case "鳥松鄉": strArea = "鳥松區"; break;
                            case "岡山鎮": strArea = "岡山區"; break;
                            case "橋頭鄉": strArea = "橋頭區"; break;
                            case "燕巢鄉": strArea = "燕巢區"; break;
                            case "田寮鄉": strArea = "田寮區"; break;
                            case "阿蓮鄉": strArea = "阿蓮區"; break;
                            case "路竹鄉": strArea = "路竹區"; break;
                            case "湖內鄉": strArea = "湖內區"; break;
                            case "茄萣鄉": strArea = "茄萣區"; break;
                            case "永安鄉": strArea = "永安區"; break;
                            case "彌陀鄉": strArea = "彌陀區"; break;
                            case "梓官鄉": strArea = "梓官區"; break;
                            case "旗山鎮": strArea = "旗山區"; break;
                            case "美濃鎮": strArea = "美濃區"; break;
                            case "六龜鄉": strArea = "六龜區"; break;
                            case "甲仙鄉": strArea = "甲仙區"; break;
                            case "杉林鄉": strArea = "杉林區"; break;
                            case "內門鄉": strArea = "內門區"; break;
                            case "茂林鄉": strArea = "茂林區"; break;
                            case "桃源鄉": strArea = "桃源區"; break;
                            case "三民鄉": strArea = "那瑪夏區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "屏東縣")
                    {
                        switch (l_Address.Region)
                        {
                            case "霧台鄉": strArea = "霧臺鄉"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "新北市")
                    {
                        switch (l_Address.Region)
                        {

                            case "板橋市": strArea = "板橋區"; break;
                            case "板橋區": strArea = "板橋區"; break;
                            case "板橋鎮": strArea = "板橋區"; break;
                            case "板橋鄉": strArea = "板橋區"; break;
                            case "三重市": strArea = "三重區"; break;
                            case "三重區": strArea = "三重區"; break;
                            case "三重鎮": strArea = "三重區"; break;
                            case "三重鄉": strArea = "三重區"; break;
                            case "中和市": strArea = "中和區"; break;
                            case "中和區": strArea = "中和區"; break;
                            case "中和鎮": strArea = "中和區"; break;
                            case "中和鄉": strArea = "中和區"; break;
                            case "永和市": strArea = "永和區"; break;
                            case "永和區": strArea = "永和區"; break;
                            case "永和鎮": strArea = "永和區"; break;
                            case "永和鄉": strArea = "永和區"; break;
                            case "新莊市": strArea = "新莊區"; break;
                            case "新莊區": strArea = "新莊區"; break;
                            case "新莊鎮": strArea = "新莊區"; break;
                            case "新莊鄉": strArea = "新莊區"; break;
                            case "新店市": strArea = "新店區"; break;
                            case "新店區": strArea = "新店區"; break;
                            case "新店鎮": strArea = "新店區"; break;
                            case "新店鄉": strArea = "新店區"; break;
                            case "樹林市": strArea = "樹林區"; break;
                            case "樹林區": strArea = "樹林區"; break;
                            case "樹林鎮": strArea = "樹林區"; break;
                            case "樹林鄉": strArea = "樹林區"; break;
                            case "鶯歌市": strArea = "鶯歌區"; break;
                            case "鶯歌區": strArea = "鶯歌區"; break;
                            case "鶯歌鎮": strArea = "鶯歌區"; break;
                            case "鶯歌鄉": strArea = "鶯歌區"; break;
                            case "三峽市": strArea = "三峽區"; break;
                            case "三峽區": strArea = "三峽區"; break;
                            case "三峽鄉": strArea = "三峽區"; break;
                            case "三峽鎮": strArea = "三峽區"; break;
                            case "淡水市": strArea = "淡水區"; break;
                            case "淡水區": strArea = "淡水區"; break;
                            case "淡水鎮": strArea = "淡水區"; break;
                            case "淡水鄉": strArea = "淡水區"; break;
                            case "汐止市": strArea = "汐止區"; break;
                            case "汐止區": strArea = "汐止區"; break;
                            case "汐止鎮": strArea = "汐止區"; break;
                            case "汐止鄉": strArea = "汐止區"; break;
                            case "瑞芳市": strArea = "瑞芳區"; break;
                            case "瑞芳區": strArea = "瑞芳區"; break;
                            case "瑞芳鎮": strArea = "瑞芳區"; break;
                            case "瑞芳鄉": strArea = "瑞芳區"; break;
                            case "土城市": strArea = "土城區"; break;
                            case "土城區": strArea = "土城區"; break;
                            case "土城鎮": strArea = "土城區"; break;
                            case "土城鄉": strArea = "土城區"; break;
                            case "蘆洲市": strArea = "蘆洲區"; break;
                            case "蘆洲區": strArea = "蘆洲區"; break;
                            case "蘆洲鎮": strArea = "蘆洲區"; break;
                            case "蘆洲鄉": strArea = "蘆洲區"; break;
                            case "五股市": strArea = "五股區"; break;
                            case "五股區": strArea = "五股區"; break;
                            case "五股鎮": strArea = "五股區"; break;
                            case "五股鄉": strArea = "五股區"; break;
                            case "泰山市": strArea = "泰山區"; break;
                            case "泰山區": strArea = "泰山區"; break;
                            case "泰山鎮": strArea = "泰山區"; break;
                            case "泰山鄉": strArea = "泰山區"; break;
                            case "林口市": strArea = "林口區"; break;
                            case "林口區": strArea = "林口區"; break;
                            case "林口鎮": strArea = "林口區"; break;
                            case "林口鄉": strArea = "林口區"; break;
                            case "深坑市": strArea = "深坑區"; break;
                            case "深坑區": strArea = "深坑區"; break;
                            case "深坑鎮": strArea = "深坑區"; break;
                            case "深坑鄉": strArea = "深坑區"; break;
                            case "石碇市": strArea = "石碇區"; break;
                            case "石碇區": strArea = "石碇區"; break;
                            case "石碇鎮": strArea = "石碇區"; break;
                            case "石碇鄉": strArea = "石碇區"; break;
                            case "坪林市": strArea = "坪林區"; break;
                            case "坪林區": strArea = "坪林區"; break;
                            case "坪林鎮": strArea = "坪林區"; break;
                            case "坪林鄉": strArea = "坪林區"; break;
                            case "三芝市": strArea = "三芝區"; break;
                            case "三芝區": strArea = "三芝區"; break;
                            case "三芝鎮": strArea = "三芝區"; break;
                            case "三芝鄉": strArea = "三芝區"; break;
                            case "石門市": strArea = "石門區"; break;
                            case "石門區": strArea = "石門區"; break;
                            case "石門鎮": strArea = "石門區"; break;
                            case "石門鄉": strArea = "石門區"; break;
                            case "八里市": strArea = "八里區"; break;
                            case "八里區": strArea = "八里區"; break;
                            case "八里鎮": strArea = "八里區"; break;
                            case "八里鄉": strArea = "八里區"; break;
                            case "平溪市": strArea = "平溪區"; break;
                            case "平溪區": strArea = "平溪區"; break;
                            case "平溪鎮": strArea = "平溪區"; break;
                            case "平溪鄉": strArea = "平溪區"; break;
                            case "雙溪市": strArea = "雙溪區"; break;
                            case "雙溪區": strArea = "雙溪區"; break;
                            case "雙溪鎮": strArea = "雙溪區"; break;
                            case "雙溪鄉": strArea = "雙溪區"; break;
                            case "貢寮市": strArea = "貢寮區"; break;
                            case "貢寮區": strArea = "貢寮區"; break;
                            case "貢寮鎮": strArea = "貢寮區"; break;
                            case "貢寮鄉": strArea = "貢寮區"; break;
                            case "金山市": strArea = "金山區"; break;
                            case "金山區": strArea = "金山區"; break;
                            case "金山鎮": strArea = "金山區"; break;
                            case "金山鄉": strArea = "金山區"; break;
                            case "萬里市": strArea = "萬里區"; break;
                            case "萬里區": strArea = "萬里區"; break;
                            case "萬里鎮": strArea = "萬里區"; break;
                            case "萬里鄉": strArea = "萬里區"; break;
                            case "烏來市": strArea = "烏來區"; break;
                            case "烏來區": strArea = "烏來區"; break;
                            case "烏來鎮": strArea = "烏來區"; break;
                            case "烏來鄉": strArea = "烏來區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else
                    {
                        strArea = l_Address.Region;
                    }
                    #endregion


                    cmd.Parameters.AddWithValue("@strCity", strCity);
                    cmd.Parameters.AddWithValue("@strArea", strArea);
                    cmd.CommandText = @"
                                            select supplier_code from ttArriveSites
                                            where post_city = @strCity
                                            and post_area  = @strArea";
                    Detaildt = dbAdapter.getDataTable(cmd);
                }
                //如果 有撈出資料 就把 supplier_code 拋回
                if (Detaildt.Rows.Count > 0)
                {

                    resultcode = true;
                    msg = Detaildt.Rows[0][0].ToString();
                }
                else
                {//如果沒有撈出資料 就拋回
                    resultcode = false;
                    msg = "999";
                }
            }
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = "";

            strAppRequest = JsonConvert.SerializeObject(new Request_Msg { isSuccess = resultcode, msg = msg }, Newtonsoft.Json.Formatting.Indented);

            return strAppRequest;
        }


        /// <summary>
        /// 依輸入地址回傳到著碼(零擔)
        /// </summary>
        /// <param name="City">縣/市</param>
        /// <param name="Area">區域</param>
        /// <returns></returns>
        [WebMethod(Description = "依輸入地址回傳到著碼(零擔)", EnableSession = true)]
        public string GetBreakBulkNumber(string address)
        {
            writeLog("call", "GetBreakBulkNumber", "GetBreakBulkNumber", "");
            DataTable dtAppRequest = new DataTable();

            Boolean resultcode = true;
            string error_msg = string.Empty;
            string msg = string.Empty;

            //正規畫過後的縣/市
            string strCity = string.Empty;

            //正規畫過後的 區
            string strArea = string.Empty;


            if (address.Equals(""))
            {
                resultcode = false;
                msg = "999";
            }
            else
            {
                DataTable Detaildt = new DataTable();

                using (SqlCommand cmd = new SqlCommand())
                {
                    //先簡體轉繁體
                    string ChinaFoTaiwan = ToTraditional(address);
                    ChinaFoTaiwan = ChinaFoTaiwan.Replace(" ", "");
                    ChinaFoTaiwan = ChinaFoTaiwan.Replace("臺灣省", "");

                    //將轉為繁體的地址拆分縣市、區
                    ADDRESS l_Address = new ADDRESS(ChinaFoTaiwan);

                    #region 縣市正規畫 
                    //如果 縣/市 
                    switch (l_Address.City)
                    {   //符合         將 臺北市 塞入 strCity
                        case "台北市": strCity = "臺北市"; break;
                        //符合         將 台中市 塞入 strCity
                        case "台中市": strCity = "臺中市"; break;
                        //符合         將 台南市 塞入 strCity
                        case "台南市": strCity = "臺南市"; break;
                        //符合         將 桃園縣 塞入 strCity
                        case "桃園縣": strCity = "桃園市"; break;
                        //符合         將 高雄縣 塞入 strCity
                        case "高雄縣": strCity = "高雄市"; break;
                        //符合         將 新北市 塞入 strCity
                        case "台北縣": strCity = "新北市"; break;
                        default:
                            strCity = l_Address.City; break;
                    }
                    #endregion


                    #region 區 正規畫
                    /* 判斷法可以用 switch case (縣正規畫的那種)
                     * 也可以使用 if else 完全看個人習慣
                     * if(符合) {
                     *      程式碼....
                     * } else if(符合){
                     *      程式碼....
                     * }
                     */
                    if (strCity == "臺中市")
                    {
                        switch (l_Address.Region)
                        {
                            case "豐原市": strArea = "豐原區"; break;
                            case "后里鄉": strArea = "后里區"; break;
                            case "潭子鄉": strArea = "潭子區"; break;
                            case "大雅鄉": strArea = "大雅區"; break;
                            case "神岡鄉": strArea = "神岡區"; break;
                            case "東勢鎮": strArea = "東勢區"; break;
                            case "石岡鄉": strArea = "石岡區"; break;
                            case "新社鄉": strArea = "新社區"; break;
                            case "和平鄉": strArea = "和平區"; break;
                            case "清水鎮": strArea = "清水區"; break;
                            case "沙鹿鎮": strArea = "沙鹿區"; break;
                            case "梧棲鎮": strArea = "梧棲區"; break;
                            case "大甲鎮": strArea = "大甲區"; break;
                            case "大安鄉": strArea = "大安區"; break;
                            case "外埔鄉": strArea = "外埔區"; break;
                            case "龍井鄉": strArea = "龍井區"; break;
                            case "大肚鄉": strArea = "大肚區"; break;
                            case "大里市": strArea = "大里區"; break;
                            case "霧峰鄉": strArea = "霧峰區"; break;
                            case "太平市": strArea = "太平區"; break;
                            case "太平鄉": strArea = "太平區"; break;
                            case "烏日鄉": strArea = "烏日區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "臺南市")
                    {
                        switch (l_Address.Region)
                        {
                            case "中區": strArea = "中西區"; break;
                            case "西區": strArea = "中西區"; break;
                            case "新化鎮": strArea = "新化區"; break;
                            case "楠西鄉": strArea = "楠西區"; break;
                            case "南化鄉": strArea = "南化區"; break;
                            case "玉井鄉": strArea = "玉井區"; break;
                            case "左鎮鄉": strArea = "左鎮區"; break;
                            case "新市鄉": strArea = "新市區"; break;
                            case "山上鄉": strArea = "山上區"; break;
                            case "龍崎鄉": strArea = "龍崎區"; break;
                            case "永康鄉": strArea = "永康區"; break;
                            case "永康市": strArea = "永康區"; break;
                            case "歸仁鄉": strArea = "歸仁區"; break;
                            case "仁德鄉": strArea = "仁德區"; break;
                            case "關廟鄉": strArea = "關廟區"; break;
                            case "安定鄉": strArea = "安定區"; break;
                            case "善化鎮": strArea = "善化區"; break;
                            case "新營市": strArea = "新營區"; break;
                            case "鹽水鎮": strArea = "鹽水區"; break;
                            case "柳營鄉": strArea = "柳營區"; break;
                            case "白河鎮": strArea = "白河區"; break;
                            case "後壁鄉": strArea = "後壁區"; break;
                            case "東山鄉": strArea = "東山區"; break;
                            case "佳里鎮": strArea = "佳里區"; break;
                            case "西港鄉": strArea = "西港區"; break;
                            case "北門鄉": strArea = "北門區"; break;
                            case "學甲鎮": strArea = "學甲區"; break;
                            case "將軍鄉": strArea = "將軍區"; break;
                            case "麻豆鎮": strArea = "麻豆區"; break;
                            case "官田鄉": strArea = "官田區"; break;
                            case "下營鄉": strArea = "下營區"; break;
                            case "六甲鄉": strArea = "六甲區"; break;
                            case "大內鄉": strArea = "大內區"; break;
                            case "七股鄉": strArea = "七股區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "桃園市")
                    {
                        switch (l_Address.Region)
                        {
                            case "觀音鄉": strArea = "觀音區"; break;
                            case "觀音鎮": strArea = "觀音區"; break;
                            case "觀音市": strArea = "觀音區"; break;
                            case "觀音區": strArea = "觀音區"; break;
                            case "中壢鄉": strArea = "中壢區"; break;
                            case "中壢鎮": strArea = "中壢區"; break;
                            case "中壢市": strArea = "中壢區"; break;
                            case "中壢區": strArea = "中壢區"; break;
                            case "平鎮鄉": strArea = "平鎮區"; break;
                            case "平鎮鎮": strArea = "平鎮區"; break;
                            case "平鎮市": strArea = "平鎮區"; break;
                            case "平鎮區": strArea = "平鎮區"; break;
                            case "桃園鄉": strArea = "桃園區"; break;
                            case "桃園鎮": strArea = "桃園區"; break;
                            case "桃園市": strArea = "桃園區"; break;
                            case "桃園區": strArea = "桃園區"; break;
                            case "龜山鄉": strArea = "龜山區"; break;
                            case "龜山鎮": strArea = "龜山區"; break;
                            case "龜山市": strArea = "龜山區"; break;
                            case "龜山區": strArea = "龜山區"; break;
                            case "八德鄉": strArea = "八德區"; break;
                            case "八德鎮": strArea = "八德區"; break;
                            case "八德市": strArea = "八德區"; break;
                            case "八德區": strArea = "八德區"; break;
                            case "大溪鄉": strArea = "大溪區"; break;
                            case "大溪鎮": strArea = "大溪區"; break;
                            case "大溪市": strArea = "大溪區"; break;
                            case "大溪區": strArea = "大溪區"; break;
                            case "復興鄉": strArea = "復興區"; break;
                            case "復興鎮": strArea = "復興區"; break;
                            case "復興市": strArea = "復興區"; break;
                            case "復興區": strArea = "復興區"; break;
                            case "大園鄉": strArea = "大園區"; break;
                            case "大園鎮": strArea = "大園區"; break;
                            case "大園市": strArea = "大園區"; break;
                            case "大園區": strArea = "大園區"; break;
                            case "蘆竹鄉": strArea = "蘆竹區"; break;
                            case "蘆竹鎮": strArea = "蘆竹區"; break;
                            case "蘆竹市": strArea = "蘆竹區"; break;
                            case "蘆竹區": strArea = "蘆竹區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "高雄市")
                    {
                        switch (l_Address.Region)
                        {
                            case "鳳山市": strArea = "鳳山區"; break;
                            case "林園鄉": strArea = "林園區"; break;
                            case "大寮鄉": strArea = "大寮區"; break;
                            case "大樹鄉": strArea = "大樹區"; break;
                            case "大社鄉": strArea = "大社區"; break;
                            case "仁武鄉": strArea = "仁武區"; break;
                            case "鳥松鄉": strArea = "鳥松區"; break;
                            case "岡山鎮": strArea = "岡山區"; break;
                            case "橋頭鄉": strArea = "橋頭區"; break;
                            case "燕巢鄉": strArea = "燕巢區"; break;
                            case "田寮鄉": strArea = "田寮區"; break;
                            case "阿蓮鄉": strArea = "阿蓮區"; break;
                            case "路竹鄉": strArea = "路竹區"; break;
                            case "湖內鄉": strArea = "湖內區"; break;
                            case "茄萣鄉": strArea = "茄萣區"; break;
                            case "永安鄉": strArea = "永安區"; break;
                            case "彌陀鄉": strArea = "彌陀區"; break;
                            case "梓官鄉": strArea = "梓官區"; break;
                            case "旗山鎮": strArea = "旗山區"; break;
                            case "美濃鎮": strArea = "美濃區"; break;
                            case "六龜鄉": strArea = "六龜區"; break;
                            case "甲仙鄉": strArea = "甲仙區"; break;
                            case "杉林鄉": strArea = "杉林區"; break;
                            case "內門鄉": strArea = "內門區"; break;
                            case "茂林鄉": strArea = "茂林區"; break;
                            case "桃源鄉": strArea = "桃源區"; break;
                            case "三民鄉": strArea = "那瑪夏區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "屏東縣")
                    {
                        switch (l_Address.Region)
                        {
                            case "霧台鄉": strArea = "霧臺鄉"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "新北市")
                    {
                        switch (l_Address.Region)
                        {

                            case "板橋市": strArea = "板橋區"; break;
                            case "板橋區": strArea = "板橋區"; break;
                            case "板橋鎮": strArea = "板橋區"; break;
                            case "板橋鄉": strArea = "板橋區"; break;
                            case "三重市": strArea = "三重區"; break;
                            case "三重區": strArea = "三重區"; break;
                            case "三重鎮": strArea = "三重區"; break;
                            case "三重鄉": strArea = "三重區"; break;
                            case "中和市": strArea = "中和區"; break;
                            case "中和區": strArea = "中和區"; break;
                            case "中和鎮": strArea = "中和區"; break;
                            case "中和鄉": strArea = "中和區"; break;
                            case "永和市": strArea = "永和區"; break;
                            case "永和區": strArea = "永和區"; break;
                            case "永和鎮": strArea = "永和區"; break;
                            case "永和鄉": strArea = "永和區"; break;
                            case "新莊市": strArea = "新莊區"; break;
                            case "新莊區": strArea = "新莊區"; break;
                            case "新莊鎮": strArea = "新莊區"; break;
                            case "新莊鄉": strArea = "新莊區"; break;
                            case "新店市": strArea = "新店區"; break;
                            case "新店區": strArea = "新店區"; break;
                            case "新店鎮": strArea = "新店區"; break;
                            case "新店鄉": strArea = "新店區"; break;
                            case "樹林市": strArea = "樹林區"; break;
                            case "樹林區": strArea = "樹林區"; break;
                            case "樹林鎮": strArea = "樹林區"; break;
                            case "樹林鄉": strArea = "樹林區"; break;
                            case "鶯歌市": strArea = "鶯歌區"; break;
                            case "鶯歌區": strArea = "鶯歌區"; break;
                            case "鶯歌鎮": strArea = "鶯歌區"; break;
                            case "鶯歌鄉": strArea = "鶯歌區"; break;
                            case "三峽市": strArea = "三峽區"; break;
                            case "三峽區": strArea = "三峽區"; break;
                            case "三峽鄉": strArea = "三峽區"; break;
                            case "三峽鎮": strArea = "三峽區"; break;
                            case "淡水市": strArea = "淡水區"; break;
                            case "淡水區": strArea = "淡水區"; break;
                            case "淡水鎮": strArea = "淡水區"; break;
                            case "淡水鄉": strArea = "淡水區"; break;
                            case "汐止市": strArea = "汐止區"; break;
                            case "汐止區": strArea = "汐止區"; break;
                            case "汐止鎮": strArea = "汐止區"; break;
                            case "汐止鄉": strArea = "汐止區"; break;
                            case "瑞芳市": strArea = "瑞芳區"; break;
                            case "瑞芳區": strArea = "瑞芳區"; break;
                            case "瑞芳鎮": strArea = "瑞芳區"; break;
                            case "瑞芳鄉": strArea = "瑞芳區"; break;
                            case "土城市": strArea = "土城區"; break;
                            case "土城區": strArea = "土城區"; break;
                            case "土城鎮": strArea = "土城區"; break;
                            case "土城鄉": strArea = "土城區"; break;
                            case "蘆洲市": strArea = "蘆洲區"; break;
                            case "蘆洲區": strArea = "蘆洲區"; break;
                            case "蘆洲鎮": strArea = "蘆洲區"; break;
                            case "蘆洲鄉": strArea = "蘆洲區"; break;
                            case "五股市": strArea = "五股區"; break;
                            case "五股區": strArea = "五股區"; break;
                            case "五股鎮": strArea = "五股區"; break;
                            case "五股鄉": strArea = "五股區"; break;
                            case "泰山市": strArea = "泰山區"; break;
                            case "泰山區": strArea = "泰山區"; break;
                            case "泰山鎮": strArea = "泰山區"; break;
                            case "泰山鄉": strArea = "泰山區"; break;
                            case "林口市": strArea = "林口區"; break;
                            case "林口區": strArea = "林口區"; break;
                            case "林口鎮": strArea = "林口區"; break;
                            case "林口鄉": strArea = "林口區"; break;
                            case "深坑市": strArea = "深坑區"; break;
                            case "深坑區": strArea = "深坑區"; break;
                            case "深坑鎮": strArea = "深坑區"; break;
                            case "深坑鄉": strArea = "深坑區"; break;
                            case "石碇市": strArea = "石碇區"; break;
                            case "石碇區": strArea = "石碇區"; break;
                            case "石碇鎮": strArea = "石碇區"; break;
                            case "石碇鄉": strArea = "石碇區"; break;
                            case "坪林市": strArea = "坪林區"; break;
                            case "坪林區": strArea = "坪林區"; break;
                            case "坪林鎮": strArea = "坪林區"; break;
                            case "坪林鄉": strArea = "坪林區"; break;
                            case "三芝市": strArea = "三芝區"; break;
                            case "三芝區": strArea = "三芝區"; break;
                            case "三芝鎮": strArea = "三芝區"; break;
                            case "三芝鄉": strArea = "三芝區"; break;
                            case "石門市": strArea = "石門區"; break;
                            case "石門區": strArea = "石門區"; break;
                            case "石門鎮": strArea = "石門區"; break;
                            case "石門鄉": strArea = "石門區"; break;
                            case "八里市": strArea = "八里區"; break;
                            case "八里區": strArea = "八里區"; break;
                            case "八里鎮": strArea = "八里區"; break;
                            case "八里鄉": strArea = "八里區"; break;
                            case "平溪市": strArea = "平溪區"; break;
                            case "平溪區": strArea = "平溪區"; break;
                            case "平溪鎮": strArea = "平溪區"; break;
                            case "平溪鄉": strArea = "平溪區"; break;
                            case "雙溪市": strArea = "雙溪區"; break;
                            case "雙溪區": strArea = "雙溪區"; break;
                            case "雙溪鎮": strArea = "雙溪區"; break;
                            case "雙溪鄉": strArea = "雙溪區"; break;
                            case "貢寮市": strArea = "貢寮區"; break;
                            case "貢寮區": strArea = "貢寮區"; break;
                            case "貢寮鎮": strArea = "貢寮區"; break;
                            case "貢寮鄉": strArea = "貢寮區"; break;
                            case "金山市": strArea = "金山區"; break;
                            case "金山區": strArea = "金山區"; break;
                            case "金山鎮": strArea = "金山區"; break;
                            case "金山鄉": strArea = "金山區"; break;
                            case "萬里市": strArea = "萬里區"; break;
                            case "萬里區": strArea = "萬里區"; break;
                            case "萬里鎮": strArea = "萬里區"; break;
                            case "萬里鄉": strArea = "萬里區"; break;
                            case "烏來市": strArea = "烏來區"; break;
                            case "烏來區": strArea = "烏來區"; break;
                            case "烏來鎮": strArea = "烏來區"; break;
                            case "烏來鄉": strArea = "烏來區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else
                    {
                        strArea = l_Address.Region;
                    }
                    #endregion


                    cmd.Parameters.AddWithValue("@strCity", strCity);
                    cmd.Parameters.AddWithValue("@strArea", strArea);
                    cmd.CommandText = @"
						select top 1 ts.station_scode from tbStation ts
						inner join ttArriveSitesScattered ta on ta.station_code = ts.station_scode
					    where ta.post_city = @strCity
                        and ta.post_area  = @strArea";
                    Detaildt = dbAdapter.getDataTable(cmd);
                }
                //如果 有撈出資料 就把 supplier_code 拋回
                if (Detaildt.Rows.Count > 0)
                {

                    resultcode = true;
                    msg = Detaildt.Rows[0][0].ToString();
                }
                else
                {//如果沒有撈出資料 就拋回
                    resultcode = false;
                    msg = "999";
                }
            }
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整

            string strAppRequest = "";

            strAppRequest = JsonConvert.SerializeObject(new Request_Msg { isSuccess = resultcode, msg = msg }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;
        }




        /// <summary>
        /// 依輸入地址回傳到著碼(零擔)
        /// </summary>
        /// <param name="City">縣/市</param>
        /// <param name="Area">區域</param>
        /// <returns></returns>
        [WebMethod(Description = "依輸入地址回傳到著碼、到著站名稱(零擔)", EnableSession = true)]
        public string GetBreakBulkNumberName(string address)
        {
            writeLog("call", "GetBreakBulkNumber", "GetBreakBulkNumberName", "");
            DataTable dtAppRequest = new DataTable();

            Boolean resultcode = true;
            string error_msg = string.Empty;
            string msg = string.Empty;

            //正規畫過後的縣/市
            string strCity = string.Empty;

            //正規畫過後的 區
            string strArea = string.Empty;


            if (address.Equals(""))
            {
                resultcode = false;
                msg = "999";
            }
            else
            {
                DataTable Detaildt = new DataTable();

                using (SqlCommand cmd = new SqlCommand())
                {
                    //先簡體轉繁體
                    string ChinaFoTaiwan = ToTraditional(address);
                    ChinaFoTaiwan = ChinaFoTaiwan.Replace(" ", "");
                    ChinaFoTaiwan = ChinaFoTaiwan.Replace("臺灣省", "");

                    //將轉為繁體的地址拆分縣市、區
                    ADDRESS l_Address = new ADDRESS(ChinaFoTaiwan);

                    #region 縣市正規畫 
                    //如果 縣/市 
                    switch (l_Address.City)
                    {   //符合         將 臺北市 塞入 strCity
                        case "台北市": strCity = "臺北市"; break;
                        //符合         將 台中市 塞入 strCity
                        case "台中市": strCity = "臺中市"; break;
                        //符合         將 台南市 塞入 strCity
                        case "台南市": strCity = "臺南市"; break;
                        //符合         將 桃園縣 塞入 strCity
                        case "桃園縣": strCity = "桃園市"; break;
                        //符合         將 高雄縣 塞入 strCity
                        case "高雄縣": strCity = "高雄市"; break;
                        //符合         將 新北市 塞入 strCity
                        case "台北縣": strCity = "新北市"; break;
                        default:
                            strCity = l_Address.City; break;
                    }
                    #endregion


                    #region 區 正規畫
                    /* 判斷法可以用 switch case (縣正規畫的那種)
                     * 也可以使用 if else 完全看個人習慣
                     * if(符合) {
                     *      程式碼....
                     * } else if(符合){
                     *      程式碼....
                     * }
                     */
                    if (strCity == "臺中市")
                    {
                        switch (l_Address.Region)
                        {
                            case "豐原市": strArea = "豐原區"; break;
                            case "后里鄉": strArea = "后里區"; break;
                            case "潭子鄉": strArea = "潭子區"; break;
                            case "大雅鄉": strArea = "大雅區"; break;
                            case "神岡鄉": strArea = "神岡區"; break;
                            case "東勢鎮": strArea = "東勢區"; break;
                            case "石岡鄉": strArea = "石岡區"; break;
                            case "新社鄉": strArea = "新社區"; break;
                            case "和平鄉": strArea = "和平區"; break;
                            case "清水鎮": strArea = "清水區"; break;
                            case "沙鹿鎮": strArea = "沙鹿區"; break;
                            case "梧棲鎮": strArea = "梧棲區"; break;
                            case "大甲鎮": strArea = "大甲區"; break;
                            case "大安鄉": strArea = "大安區"; break;
                            case "外埔鄉": strArea = "外埔區"; break;
                            case "龍井鄉": strArea = "龍井區"; break;
                            case "大肚鄉": strArea = "大肚區"; break;
                            case "大里市": strArea = "大里區"; break;
                            case "霧峰鄉": strArea = "霧峰區"; break;
                            case "太平市": strArea = "太平區"; break;
                            case "太平鄉": strArea = "太平區"; break;
                            case "烏日鄉": strArea = "烏日區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "臺南市")
                    {
                        switch (l_Address.Region)
                        {
                            case "中區": strArea = "中西區"; break;
                            case "西區": strArea = "中西區"; break;
                            case "新化鎮": strArea = "新化區"; break;
                            case "楠西鄉": strArea = "楠西區"; break;
                            case "南化鄉": strArea = "南化區"; break;
                            case "玉井鄉": strArea = "玉井區"; break;
                            case "左鎮鄉": strArea = "左鎮區"; break;
                            case "新市鄉": strArea = "新市區"; break;
                            case "山上鄉": strArea = "山上區"; break;
                            case "龍崎鄉": strArea = "龍崎區"; break;
                            case "永康鄉": strArea = "永康區"; break;
                            case "永康市": strArea = "永康區"; break;
                            case "歸仁鄉": strArea = "歸仁區"; break;
                            case "仁德鄉": strArea = "仁德區"; break;
                            case "關廟鄉": strArea = "關廟區"; break;
                            case "安定鄉": strArea = "安定區"; break;
                            case "善化鎮": strArea = "善化區"; break;
                            case "新營市": strArea = "新營區"; break;
                            case "鹽水鎮": strArea = "鹽水區"; break;
                            case "柳營鄉": strArea = "柳營區"; break;
                            case "白河鎮": strArea = "白河區"; break;
                            case "後壁鄉": strArea = "後壁區"; break;
                            case "東山鄉": strArea = "東山區"; break;
                            case "佳里鎮": strArea = "佳里區"; break;
                            case "西港鄉": strArea = "西港區"; break;
                            case "北門鄉": strArea = "北門區"; break;
                            case "學甲鎮": strArea = "學甲區"; break;
                            case "將軍鄉": strArea = "將軍區"; break;
                            case "麻豆鎮": strArea = "麻豆區"; break;
                            case "官田鄉": strArea = "官田區"; break;
                            case "下營鄉": strArea = "下營區"; break;
                            case "六甲鄉": strArea = "六甲區"; break;
                            case "大內鄉": strArea = "大內區"; break;
                            case "七股鄉": strArea = "七股區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "桃園市")
                    {
                        switch (l_Address.Region)
                        {
                            case "觀音鄉": strArea = "觀音區"; break;
                            case "觀音鎮": strArea = "觀音區"; break;
                            case "觀音市": strArea = "觀音區"; break;
                            case "觀音區": strArea = "觀音區"; break;
                            case "中壢鄉": strArea = "中壢區"; break;
                            case "中壢鎮": strArea = "中壢區"; break;
                            case "中壢市": strArea = "中壢區"; break;
                            case "中壢區": strArea = "中壢區"; break;
                            case "平鎮鄉": strArea = "平鎮區"; break;
                            case "平鎮鎮": strArea = "平鎮區"; break;
                            case "平鎮市": strArea = "平鎮區"; break;
                            case "平鎮區": strArea = "平鎮區"; break;
                            case "桃園鄉": strArea = "桃園區"; break;
                            case "桃園鎮": strArea = "桃園區"; break;
                            case "桃園市": strArea = "桃園區"; break;
                            case "桃園區": strArea = "桃園區"; break;
                            case "龜山鄉": strArea = "龜山區"; break;
                            case "龜山鎮": strArea = "龜山區"; break;
                            case "龜山市": strArea = "龜山區"; break;
                            case "龜山區": strArea = "龜山區"; break;
                            case "八德鄉": strArea = "八德區"; break;
                            case "八德鎮": strArea = "八德區"; break;
                            case "八德市": strArea = "八德區"; break;
                            case "八德區": strArea = "八德區"; break;
                            case "大溪鄉": strArea = "大溪區"; break;
                            case "大溪鎮": strArea = "大溪區"; break;
                            case "大溪市": strArea = "大溪區"; break;
                            case "大溪區": strArea = "大溪區"; break;
                            case "復興鄉": strArea = "復興區"; break;
                            case "復興鎮": strArea = "復興區"; break;
                            case "復興市": strArea = "復興區"; break;
                            case "復興區": strArea = "復興區"; break;
                            case "大園鄉": strArea = "大園區"; break;
                            case "大園鎮": strArea = "大園區"; break;
                            case "大園市": strArea = "大園區"; break;
                            case "大園區": strArea = "大園區"; break;
                            case "蘆竹鄉": strArea = "蘆竹區"; break;
                            case "蘆竹鎮": strArea = "蘆竹區"; break;
                            case "蘆竹市": strArea = "蘆竹區"; break;
                            case "蘆竹區": strArea = "蘆竹區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "高雄市")
                    {
                        switch (l_Address.Region)
                        {
                            case "鳳山市": strArea = "鳳山區"; break;
                            case "林園鄉": strArea = "林園區"; break;
                            case "大寮鄉": strArea = "大寮區"; break;
                            case "大樹鄉": strArea = "大樹區"; break;
                            case "大社鄉": strArea = "大社區"; break;
                            case "仁武鄉": strArea = "仁武區"; break;
                            case "鳥松鄉": strArea = "鳥松區"; break;
                            case "岡山鎮": strArea = "岡山區"; break;
                            case "橋頭鄉": strArea = "橋頭區"; break;
                            case "燕巢鄉": strArea = "燕巢區"; break;
                            case "田寮鄉": strArea = "田寮區"; break;
                            case "阿蓮鄉": strArea = "阿蓮區"; break;
                            case "路竹鄉": strArea = "路竹區"; break;
                            case "湖內鄉": strArea = "湖內區"; break;
                            case "茄萣鄉": strArea = "茄萣區"; break;
                            case "永安鄉": strArea = "永安區"; break;
                            case "彌陀鄉": strArea = "彌陀區"; break;
                            case "梓官鄉": strArea = "梓官區"; break;
                            case "旗山鎮": strArea = "旗山區"; break;
                            case "美濃鎮": strArea = "美濃區"; break;
                            case "六龜鄉": strArea = "六龜區"; break;
                            case "甲仙鄉": strArea = "甲仙區"; break;
                            case "杉林鄉": strArea = "杉林區"; break;
                            case "內門鄉": strArea = "內門區"; break;
                            case "茂林鄉": strArea = "茂林區"; break;
                            case "桃源鄉": strArea = "桃源區"; break;
                            case "三民鄉": strArea = "那瑪夏區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "屏東縣")
                    {
                        switch (l_Address.Region)
                        {
                            case "霧台鄉": strArea = "霧臺鄉"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else if (strCity == "新北市")
                    {
                        switch (l_Address.Region)
                        {

                            case "板橋市": strArea = "板橋區"; break;
                            case "板橋區": strArea = "板橋區"; break;
                            case "板橋鎮": strArea = "板橋區"; break;
                            case "板橋鄉": strArea = "板橋區"; break;
                            case "三重市": strArea = "三重區"; break;
                            case "三重區": strArea = "三重區"; break;
                            case "三重鎮": strArea = "三重區"; break;
                            case "三重鄉": strArea = "三重區"; break;
                            case "中和市": strArea = "中和區"; break;
                            case "中和區": strArea = "中和區"; break;
                            case "中和鎮": strArea = "中和區"; break;
                            case "中和鄉": strArea = "中和區"; break;
                            case "永和市": strArea = "永和區"; break;
                            case "永和區": strArea = "永和區"; break;
                            case "永和鎮": strArea = "永和區"; break;
                            case "永和鄉": strArea = "永和區"; break;
                            case "新莊市": strArea = "新莊區"; break;
                            case "新莊區": strArea = "新莊區"; break;
                            case "新莊鎮": strArea = "新莊區"; break;
                            case "新莊鄉": strArea = "新莊區"; break;
                            case "新店市": strArea = "新店區"; break;
                            case "新店區": strArea = "新店區"; break;
                            case "新店鎮": strArea = "新店區"; break;
                            case "新店鄉": strArea = "新店區"; break;
                            case "樹林市": strArea = "樹林區"; break;
                            case "樹林區": strArea = "樹林區"; break;
                            case "樹林鎮": strArea = "樹林區"; break;
                            case "樹林鄉": strArea = "樹林區"; break;
                            case "鶯歌市": strArea = "鶯歌區"; break;
                            case "鶯歌區": strArea = "鶯歌區"; break;
                            case "鶯歌鎮": strArea = "鶯歌區"; break;
                            case "鶯歌鄉": strArea = "鶯歌區"; break;
                            case "三峽市": strArea = "三峽區"; break;
                            case "三峽區": strArea = "三峽區"; break;
                            case "三峽鄉": strArea = "三峽區"; break;
                            case "三峽鎮": strArea = "三峽區"; break;
                            case "淡水市": strArea = "淡水區"; break;
                            case "淡水區": strArea = "淡水區"; break;
                            case "淡水鎮": strArea = "淡水區"; break;
                            case "淡水鄉": strArea = "淡水區"; break;
                            case "汐止市": strArea = "汐止區"; break;
                            case "汐止區": strArea = "汐止區"; break;
                            case "汐止鎮": strArea = "汐止區"; break;
                            case "汐止鄉": strArea = "汐止區"; break;
                            case "瑞芳市": strArea = "瑞芳區"; break;
                            case "瑞芳區": strArea = "瑞芳區"; break;
                            case "瑞芳鎮": strArea = "瑞芳區"; break;
                            case "瑞芳鄉": strArea = "瑞芳區"; break;
                            case "土城市": strArea = "土城區"; break;
                            case "土城區": strArea = "土城區"; break;
                            case "土城鎮": strArea = "土城區"; break;
                            case "土城鄉": strArea = "土城區"; break;
                            case "蘆洲市": strArea = "蘆洲區"; break;
                            case "蘆洲區": strArea = "蘆洲區"; break;
                            case "蘆洲鎮": strArea = "蘆洲區"; break;
                            case "蘆洲鄉": strArea = "蘆洲區"; break;
                            case "五股市": strArea = "五股區"; break;
                            case "五股區": strArea = "五股區"; break;
                            case "五股鎮": strArea = "五股區"; break;
                            case "五股鄉": strArea = "五股區"; break;
                            case "泰山市": strArea = "泰山區"; break;
                            case "泰山區": strArea = "泰山區"; break;
                            case "泰山鎮": strArea = "泰山區"; break;
                            case "泰山鄉": strArea = "泰山區"; break;
                            case "林口市": strArea = "林口區"; break;
                            case "林口區": strArea = "林口區"; break;
                            case "林口鎮": strArea = "林口區"; break;
                            case "林口鄉": strArea = "林口區"; break;
                            case "深坑市": strArea = "深坑區"; break;
                            case "深坑區": strArea = "深坑區"; break;
                            case "深坑鎮": strArea = "深坑區"; break;
                            case "深坑鄉": strArea = "深坑區"; break;
                            case "石碇市": strArea = "石碇區"; break;
                            case "石碇區": strArea = "石碇區"; break;
                            case "石碇鎮": strArea = "石碇區"; break;
                            case "石碇鄉": strArea = "石碇區"; break;
                            case "坪林市": strArea = "坪林區"; break;
                            case "坪林區": strArea = "坪林區"; break;
                            case "坪林鎮": strArea = "坪林區"; break;
                            case "坪林鄉": strArea = "坪林區"; break;
                            case "三芝市": strArea = "三芝區"; break;
                            case "三芝區": strArea = "三芝區"; break;
                            case "三芝鎮": strArea = "三芝區"; break;
                            case "三芝鄉": strArea = "三芝區"; break;
                            case "石門市": strArea = "石門區"; break;
                            case "石門區": strArea = "石門區"; break;
                            case "石門鎮": strArea = "石門區"; break;
                            case "石門鄉": strArea = "石門區"; break;
                            case "八里市": strArea = "八里區"; break;
                            case "八里區": strArea = "八里區"; break;
                            case "八里鎮": strArea = "八里區"; break;
                            case "八里鄉": strArea = "八里區"; break;
                            case "平溪市": strArea = "平溪區"; break;
                            case "平溪區": strArea = "平溪區"; break;
                            case "平溪鎮": strArea = "平溪區"; break;
                            case "平溪鄉": strArea = "平溪區"; break;
                            case "雙溪市": strArea = "雙溪區"; break;
                            case "雙溪區": strArea = "雙溪區"; break;
                            case "雙溪鎮": strArea = "雙溪區"; break;
                            case "雙溪鄉": strArea = "雙溪區"; break;
                            case "貢寮市": strArea = "貢寮區"; break;
                            case "貢寮區": strArea = "貢寮區"; break;
                            case "貢寮鎮": strArea = "貢寮區"; break;
                            case "貢寮鄉": strArea = "貢寮區"; break;
                            case "金山市": strArea = "金山區"; break;
                            case "金山區": strArea = "金山區"; break;
                            case "金山鎮": strArea = "金山區"; break;
                            case "金山鄉": strArea = "金山區"; break;
                            case "萬里市": strArea = "萬里區"; break;
                            case "萬里區": strArea = "萬里區"; break;
                            case "萬里鎮": strArea = "萬里區"; break;
                            case "萬里鄉": strArea = "萬里區"; break;
                            case "烏來市": strArea = "烏來區"; break;
                            case "烏來區": strArea = "烏來區"; break;
                            case "烏來鎮": strArea = "烏來區"; break;
                            case "烏來鄉": strArea = "烏來區"; break;
                            default:
                                strArea = l_Address.Region; break;
                        }
                    }
                    else
                    {
                        strArea = l_Address.Region;
                    }
                    #endregion


                    cmd.Parameters.AddWithValue("@strCity", strCity);
                    cmd.Parameters.AddWithValue("@strArea", strArea);
                    cmd.CommandText = @"
						select top 1 ts.station_scode,ts.station_name from tbStation ts
						inner join ttArriveSitesScattered ta on ta.station_code = ts.station_scode
					    where ta.post_city = @strCity
                        and ta.post_area  = @strArea";
                    Detaildt = dbAdapter.getDataTable(cmd);
                }
                //如果 有撈出資料 就把 supplier_code 拋回
                if (Detaildt.Rows.Count > 0)
                {

                    resultcode = true;
                    msg = Detaildt.Rows[0][0].ToString() + "," + Detaildt.Rows[0][1].ToString();
                }
                else
                {//如果沒有撈出資料 就拋回
                    resultcode = false;
                    msg = "999";
                }
            }
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整

            string strAppRequest = "";

            strAppRequest = JsonConvert.SerializeObject(new Request_Msg { isSuccess = resultcode, msg = msg }, Newtonsoft.Json.Formatting.Indented);
            return strAppRequest;
        }



        public byte[] MakeBarcodeImage(string datastring)
        {
            string sCode = String.Empty;

            System.IO.MemoryStream oStream = new System.IO.MemoryStream();
            try
            {
                System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring);
                oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
                oimg.Dispose();
                return oStream.ToArray();
            }
            finally
            {

                oStream.Dispose();
            }
        }

        public static System.Drawing.Image GenerateBarCodeBitmap(string content)
        {

            using (var barcode = new Barcode()
            {

                IncludeLabel = true,
                Alignment = AlignmentPositions.CENTER,
                Width = 250,
                Height = 50,
                LabelFont = new Font("verdana", 10f),
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
                ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式

            })
            {
                return barcode.Encode(TYPE.Codabar, content);
            }
        }


        #region 執行歷程寫log到資料庫ttAppLog
        /// <summary>
        /// 
        /// </summary> 
        protected void writeLog(string type, string fun_name, string memo, string log)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {

                    string sql = "Insert into ttAppLog (app_project, app_fun_name, app_type, app_memo, app_log, check_number, customer_code) values (@app_project, @app_fun_name, @app_type, @app_memo, @app_log, @check_number, @customer_code)";

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        conn.Open();

                        command.Parameters.Add("@app_project", SqlDbType.VarChar).Value = project_name;
                        command.Parameters.Add("@app_fun_name", SqlDbType.VarChar).Value = fun_name;
                        command.Parameters.Add("@app_type", SqlDbType.VarChar).Value = type;
                        command.Parameters.Add("@app_memo", SqlDbType.VarChar).Value = memo;
                        command.Parameters.Add("@app_log", SqlDbType.VarChar).Value = log;
                        command.ExecuteNonQuery();

                    }
                    conn.Close();

                }

            }
            catch (Exception e)
            {
                //這邊例外, 一樣會寫DB, 但不會再第二次catch了.
                if (!fun_name.Equals("writeLog"))
                    writeLog("err_err", "writeLog", memo, e.Message);
            }
            finally
            {
            }

        }
        #endregion

        protected void writeLogForEDI(string type, string fun_name, string memo, string app_log, string check_number, string customer_code, Boolean resultcode, DateTime start_time, DateTime end_time)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {

                    string sql = "Insert into ttAppLog (start_time , end_time, resultcode,app_project, app_fun_name, app_type, app_memo, app_log, check_number, customer_code) values (@start_time , @end_time, @resultcode,@app_project, @app_fun_name, @app_type, @app_memo, @app_log, @check_number, @customer_code)";

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        conn.Open();

                        command.Parameters.Add("@app_project", SqlDbType.VarChar).Value = project_name;
                        command.Parameters.Add("@app_fun_name", SqlDbType.VarChar).Value = fun_name;
                        command.Parameters.Add("@app_type", SqlDbType.VarChar).Value = type;
                        command.Parameters.Add("@app_memo", SqlDbType.VarChar).Value = memo;
                        command.Parameters.Add("@app_log", SqlDbType.VarChar).Value = app_log;
                        command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                        command.Parameters.Add("@customer_code", SqlDbType.VarChar).Value = customer_code;
                        command.Parameters.Add("@resultcode", SqlDbType.VarChar).Value = resultcode.ToString();
                        command.Parameters.Add("@start_time", SqlDbType.DateTime).Value = start_time;
                        command.Parameters.Add("@end_time", SqlDbType.DateTime).Value = end_time;
                        command.ExecuteNonQuery();

                    }
                    conn.Close();

                }

            }
            catch (Exception e)
            {
                //這邊例外, 一樣會寫DB, 但不會再第二次catch了.
                if (!fun_name.Equals("writeLog"))
                    writeLogForEDI("err_err", "writeLog", memo, e.Message, "", "", resultcode, start_time, end_time);
            }
            finally
            {
            }

        }


        public static bool IsHolidays(DateTime date)
        {
            // 週休二日
            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }
            // 國定假日(國曆)
            if (date.ToString("MM/dd").Equals("01/01"))
            {
                return true;
            }
            if (date.ToString("MM/dd").Equals("02/28"))
            {
                return true;
            }
            if (date.ToString("MM/dd").Equals("04/04"))
            {
                return true;
            }
            if (date.ToString("MM/dd").Equals("04/05"))
            {
                return true;
            }
            if (date.ToString("MM/dd").Equals("05/01"))
            {
                return true;
            }
            if (date.ToString("MM/dd").Equals("10/10"))
            {
                return true;
            }

            // 國定假日(農曆)
            if (GetLeapDate(date, false).Equals("12/" + GetDaysInLeapMonth(date)))
            {
                return true;
            }
            if (GetLeapDate(date, false).Equals("1/1"))
            {
                return true;
            }
            if (GetLeapDate(date, false).Equals("1/2"))
            {
                return true;
            }
            if (GetLeapDate(date, false).Equals("1/3"))
            {
                return true;
            }
            if (GetLeapDate(date, false).Equals("1/4"))
            {
                return true;
            }
            if (GetLeapDate(date, false).Equals("1/5"))
            {
                return true;
            }
            if (GetLeapDate(date, false).Equals("5/5"))
            {
                return true;
            }
            if (GetLeapDate(date, false).Equals("8/15"))
            {
                return true;
            }


            //公司假日
            if (date.ToString("yyyy/MM/dd").Equals("2018/04/06"))
            {
                return true;
            }
            if (date.ToString("yyyy/MM/dd").Equals("2018/04/07"))
            {
                return true;
            }
            if (date.ToString("yyyy/MM/dd").Equals("2018/04/08"))
            {
                return true;
            }

            //// 公司假日
            ////日期是否在特定節日，資料庫有資料則為假日
            if (CheckHoliday(date))
            {
                return true;
            }
            ////日期是否在公司設定節日，資料庫有資料則為假日
            //if (CheckDeductionSh(date))
            //{
            //    return true;
            //}

            return false;
        }

        ///<summary>
        /// 取得農曆日期
        ///</summary>
        public static string GetLeapDate(DateTime Date, bool bShowYear = true, bool bShowMonth = true)
        {
            int L_ly, nLeapYear, nLeapMonth;

            ChineseLunisolarCalendar MyChineseLunisolarCalendar = new ChineseLunisolarCalendar();

            nLeapYear = MyChineseLunisolarCalendar.GetYear(Date);
            nLeapMonth = MyChineseLunisolarCalendar.GetMonth(Date);
            if (MyChineseLunisolarCalendar.IsLeapYear(nLeapYear)) //判斷此農曆年是否為閏年
            {
                L_ly = MyChineseLunisolarCalendar.GetLeapMonth(nLeapYear); //抓出此閏年閏何月

                if (nLeapMonth >= L_ly)
                {
                    nLeapMonth--;
                }
            }
            else
            {
                nLeapMonth = MyChineseLunisolarCalendar.GetMonth(Date);
            }

            if (bShowYear)
            {
                return "" + MyChineseLunisolarCalendar.GetYear(Date) + "/" + nLeapMonth + "/" + MyChineseLunisolarCalendar.GetDayOfMonth(Date);
            }
            else if (bShowMonth)
            {
                return "" + nLeapMonth + "/" + MyChineseLunisolarCalendar.GetDayOfMonth(Date);
            }
            else
            {
                return "" + MyChineseLunisolarCalendar.GetDayOfMonth(Date);
            }
        }

        /// <summary>
        /// 判斷日期是否為公司假日
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool CheckHoliday(DateTime date)
        {
            bool RetVal = false;
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.CommandText = string.Format("SELECT CASE KindOfDay WHEN 'WORKDAY' THEN 0 ELSE 1 END  AS HOLIDAY  FROM tbCalendar where Date =  @Date");
            cmd.Parameters.AddWithValue("@Date", date);
            dt = dbAdapter.getDataTable(cmd);
            if (dt != null && dt.Rows.Count > 0)
            {
                RetVal = Convert.ToBoolean(dt.Rows[0]["HOLIDAY"]);
            }
            return RetVal;

        }

        ///<summary>
        /// 取得某一日期的該農曆月份的總天數
        ///</summary>
        public static string GetDaysInLeapMonth(DateTime date)
        {
            ChineseLunisolarCalendar MyChineseLunisolarCalendar = new ChineseLunisolarCalendar();

            return "" + MyChineseLunisolarCalendar.GetDaysInMonth(MyChineseLunisolarCalendar.GetYear(date), date.Month);
        }

        private Image BufferToImage(byte[] Buffer)
        {
            byte[] data = null;
            Image oImage = null;
            MemoryStream oMemoryStream = null;
            Bitmap oBitmap = null;
            //建立副本
            data = (byte[])Buffer.Clone();
            try
            {
                oMemoryStream = new MemoryStream(data);
                //設定資料流位置
                oMemoryStream.Position = 0;
                oImage = System.Drawing.Image.FromStream(oMemoryStream);
                //建立副本
                oBitmap = new Bitmap(oImage);
            }
            catch
            {
                throw;
            }
            finally
            {
                oMemoryStream.Close();
                oMemoryStream.Dispose();
                oMemoryStream = null;
            }
            //return oImage;
            return oBitmap;
        }



        /// <summary>
        /// 繁體轉簡體
        /// </summary>
        /// <param name="pSource">要轉換的繁體字：體</param>
        /// <returns>轉換後的簡體字：體</returns>
        public static string ToSimplified(string pSource)
        {
            String tTarget = new String(' ', pSource.Length);
            int tReturn = LCMapString(LOCALE_SYSTEM_DEFAULT, LCMAP_SIMPLIFIED_CHINESE, pSource, pSource.Length, tTarget, pSource.Length);
            return tTarget;
        }

        /// <summary>
        /// 簡體轉繁體
        /// </summary>
        /// <param name="pSource">要轉換的繁體字：體</param>
        /// <returns>轉換後的簡體字：體</returns>
        public static string ToTraditional(string pSource)
        {
            String tTarget = new String(' ', pSource.Length);
            int tReturn = LCMapString(LOCALE_SYSTEM_DEFAULT, LCMAP_TRADITIONAL_CHINESE, pSource, pSource.Length, tTarget, pSource.Length);
            return tTarget;
        }


        public class Token
        {
            //Token
            public string access_token { get; set; }
            //Refresh Token
            public string refresh_token { get; set; }
            //幾秒過期
            public int expires_in { get; set; }
        }
        public class Payload
        {
            //使用者資訊
            public User info { get; set; }
            //過期時間
            public long exp { get; set; }
        }
        public class User
        {
            public string account_code { get; set; }

        }

        //產生 HMACSHA256 雜湊
        public static string ComputeHMACSHA256(string data, string key)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            using (var hmacSHA = new HMACSHA256(keyBytes))
            {
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var hash = hmacSHA.ComputeHash(dataBytes, 0, dataBytes.Length);
                return BitConverter.ToString(hash).Replace("-", "").ToUpper();
            }
        }

        //AES 加密
        public static string AESEncrypt(string data, string key, string iv)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            var ivBytes = Encoding.UTF8.GetBytes(iv);
            var dataBytes = Encoding.UTF8.GetBytes(data);
            using (var aes = Aes.Create())
            {
                aes.Key = keyBytes;
                aes.IV = ivBytes;
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                var encryptor = aes.CreateEncryptor();
                var encrypt = encryptor
                    .TransformFinalBlock(dataBytes, 0, dataBytes.Length);
                return Convert.ToBase64String(encrypt);
            }
        }

        //AES 解密
        public static string AESDecrypt(string data, string key, string iv)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            var ivBytes = Encoding.UTF8.GetBytes(iv);
            var dataBytes = Convert.FromBase64String(data);
            using (var aes = Aes.Create())
            {
                aes.Key = keyBytes;
                aes.IV = ivBytes;
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                var decryptor = aes.CreateDecryptor();
                var decrypt = decryptor
                    .TransformFinalBlock(dataBytes, 0, dataBytes.Length);
                return Encoding.UTF8.GetString(decrypt);
            }
        }

        ///MD5加密
        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private void DoContextResponse(string Code)
        {
            Code = Code.Replace("\\", "\\\\");
            Code = Code.Replace("\r\n", "\\r\\n");
            Code = Code.Replace("'", "\'");
            Context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(Code);
            Context.Response.End();
        }

        private AddressParsingInfo PreGetAddressParsing(string address, string customCode, string comeform)
        {
            //return new Tool().GetAddressParsing(address, customCode, comeform);
            AddressParsingInfo addressParse = new AddressParsingInfo();
            var tArr = new List<Task>();

            var t = Task.Factory.StartNew(() =>
            {
                return addressParse = GetAddressParsing(address, customCode, comeform);
            });
            tArr.Add(t);
            Task.WaitAll(tArr.ToArray(), 2000);

            return addressParse;
        }

        private SpecialAreaManage PreGetSpecialAreaFee(string address, string customCode, string comeform)
        {
            //return new Tool().GetAddressParsing(address, customCode, comeform);
            SpecialAreaManage addressParse = new SpecialAreaManage();
            var tArr = new List<Task>();

            var t = Task.Factory.StartNew(() =>
            {
                return addressParse = GetSpecialAreaFee(address, customCode, comeform);
            });
            tArr.Add(t);
            Task.WaitAll(tArr.ToArray(), 2000);

            return addressParse;
        }

        private static AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
        {
            AddressParsingInfo Info = new AddressParsingInfo();
            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
            request.Timeout = 5000;
            var response = client.Post<ResData<AddressParsingInfo>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    Info = response.Data.Data;
                }
                catch (Exception e)
                {
                    Info = null;
                }

            }
            else
            {

            }

            return Info;
        }

        public SpecialAreaManage GetSpecialAreaFee(string Address, string CustomerCode, string ComeFrom, string Type = "1")
        {
            SpecialAreaManage Info = new SpecialAreaManage();
            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetSpecialAreaDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            var request = new RestRequest(Method.POST);
            request.Timeout = 5000;
            request.ReadWriteTimeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            var response = client.Post<ResData<SpecialAreaManage>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    Info = response.Data.Data;


                }
                catch (Exception e)
                {
                    throw e;

                }

            }
            else
            {
                throw new Exception("地址解析有誤");
            }

            return Info;
        }



        public class ResData<TData> : ResData
        {

            public ResData()
                : base()
            {
            }
            public ResData(ResponseCodeEnum status)
                : base(status)
            {
            }
            public ResData(ResponseCodeEnum status, string msg)
                : base(status, msg)
            {
            }
            /// <summary>
            /// 結果
            /// </summary>
            public TData Data { get; set; }
        }
        public class ResData
        {
            private ResponseCodeEnum _status;
            private string _message;

            public ResData()
            {
                _status = ResponseCodeEnum.Success;

            }

            public ResData(ResponseCodeEnum status)
            {
                _status = status;
                //_message = status.GetString();

            }

            public ResData(ResponseCodeEnum status, string msg)
            {
                _status = status;
                _message = msg;

            }
            /// <summary>
            /// 結果狀態 0成功
            /// </summary>
            public ResponseCodeEnum Status
            {
                get { return _status; }
                set { _status = value; }
            }


            /// <summary>
            /// 消息 用於調試
            /// </summary>
            public string Message
            {
                get
                {

                    return _message;
                }
                set { _message = value; }
            }

            public enum ResponseCodeEnum
            {
                #region 系統相關
                Success = 200,//成功
                Error = 500,//失敗
                #endregion


            }

        }

        #region "Json"

        /// <summary>
        /// DataTable 轉成 JSON
        /// </summary>
        /// <param name="DT"></param>
        /// <returns></returns>
        public static string DoDataTableToJson(System.Data.DataTable DT)
        {
            string Res = "{";

            try
            {


                if (DT.Rows.Count > 0)
                {
                    Res += "\"DATA\":[";
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        for (int j = 0; j < DT.Columns.Count; j++)
                        {
                            if (j == 0) Res += " {";
                            Res += string.Format("\"{0}\":\"{1}\"", DT.Columns[j].ColumnName, DT.Rows[i][j].ToString());
                            if (j != DT.Columns.Count - 1)
                            {
                                Res += ",";
                            }
                            else
                            {
                                Res += "}";
                            }
                        }

                        if (i != DT.Rows.Count - 1) Res += ", ";

                    }

                    Res += " ]";

                }

                Res += "}";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Res;
        }

        /// <summary>
        /// HashTable 轉成 JSON
        /// </summary>
        /// <param name="HT"></param>
        /// <returns></returns>
        public string DoHashTableToJson(System.Collections.Hashtable HT)
        {
            string Res = "{";

            if (HT.Count > 0)
            {
                Res += "\"DATA\":[";

                string strTmp = DoHashTableToString(HT);
                if (strTmp == "")
                    return "";
                Res += strTmp;

                Res += "]";
            }

            Res += "}";
            return Res;
        }


        public string DoHashTableToString(System.Collections.Hashtable HT)
        {
            string Res = "";

            if (HT.Count > 0)
            {
                Res += "{";

                int i = 0;
                foreach (string _key in HT.Keys)
                {
                    object item = HT[_key];

                    if (item != null)
                    {
                        switch (item.GetType().ToString())
                        {
                            case "System.Collections.Hashtable":
                                {
                                    string strTmp = DoHashTableToString((System.Collections.Hashtable)item);
                                    strTmp = string.Format("\"{0}\":[{1}]", _key, strTmp);
                                    Res += strTmp;
                                    break;
                                }

                            case "System.Collections.ArrayList":
                                {
                                    string strTmp = DoArrayListToString((System.Collections.ArrayList)item);
                                    strTmp = string.Format("\"{0}\":[{1}]", _key, strTmp);
                                    Res += strTmp;
                                    break;
                                }

                            case "System.String":
                            case "System.Boolean":
                                {
                                    string strTmp = "";
                                    strTmp = string.Format("\"{0}\":\"{1}\"", _key, item.ToString());
                                    Res += strTmp;
                                    break;
                                }

                            case "System.Data.DataTable":
                                {
                                    string strTmp = "";
                                    strTmp = string.Format("\"{0}\":[{1}]", _key, DoArrayListToString(DoDataTableToArrayList((System.Data.DataTable)item)));
                                    Res += strTmp;
                                    break;
                                }

                            default:
                                {
                                    try
                                    {
                                        string strTmp = "";
                                        strTmp = string.Format("\"{0}\":\"{1}\"", _key, item.ToString());
                                        Res += strTmp;
                                    }
                                    catch (Exception ex)
                                    {
                                        return "";
                                    }

                                    break;
                                }
                        }
                    }
                    else
                    {
                        string strTmp = "";
                        strTmp = string.Format("\"{0}\":null", _key);
                        Res += strTmp;
                    }

                    if (i != HT.Count - 1) Res += ",";
                    i++;
                }

                Res += "}";
            }

            return Res;
        }


        public string DoArrayListToString(System.Collections.ArrayList HT)
        {
            string Res = "";

            if (HT.Count > 0)
            {
                // Res &= "{"

                for (int i = 0; i <= HT.Count - 1; i++)
                {
                    switch (HT[i].GetType().ToString())
                    {
                        case "System.Collections.Hashtable":
                            {
                                string strTmp = DoHashTableToString((System.Collections.Hashtable)HT[i]);
                                Res += strTmp;
                                break;
                            }

                        case "System.Collections.ArrayList":
                            {
                                string strTmp = DoArrayListToString((System.Collections.ArrayList)HT[i]);
                                Res += strTmp;
                                break;
                            }

                        case "System.String":
                        case "System.Boolean":
                            {
                                string strTmp = "";
                                strTmp = string.Format("\"{0}\"", HT[i].ToString());
                                Res += strTmp;
                                break;
                            }

                        default:
                            {
                                try
                                {
                                    string strTmp = "";
                                    strTmp = string.Format("\"{0}\"", HT[i].ToString());
                                    Res += strTmp;
                                }
                                catch (Exception ex)
                                {
                                    return "";
                                }

                                break;
                            }
                    }

                    if (i != HT.Count - 1)
                        Res += ",";
                }
            }

            return Res;
        }


        public System.Collections.ArrayList DoDataTableToArrayList(System.Data.DataTable DT)
        {
            if (DT == null)
                return null/* TODO Change to default(_) if this is not a reference type */;
            if (DT.Rows.Count == 0)
                return null/* TODO Change to default(_) if this is not a reference type */;

            System.Collections.ArrayList Res = new System.Collections.ArrayList();

            foreach (System.Data.DataRow _row in DT.Rows)
            {
                System.Collections.Hashtable _ht = new System.Collections.Hashtable();
                for (int i = 0; i <= DT.Columns.Count - 1; i++)
                    _ht.Add(DT.Columns[i].ColumnName, _row[DT.Columns[i].ColumnName].ToString().Trim());
                Res.Add(_ht);
            }

            return Res;
        }

        #endregion
    }




    public class Introduction
    {
        /// <summary> 託運單號碼 </summary>
        public string WayBillId { get; set; }

        /// <summary> WMS訂單編號 </summary>
        public string OrderKey { get; set; }

        /// <summary> 客戶訂單號碼 </summary>
        public string ExternOrderKey { get; set; }

        /// <summary> 客戶契客代號 </summary>
        public string ContractId { get; set; }

        /// <summary> 收貨人代號 </summary>
        public string ReceiverId { get; set; }

        /// <summary> 收貨人名稱 </summary>
        public string ReceiverName { get; set; }

        /// <summary> 收貨人電話1 </summary>
        public string ReceiverTel1 { get; set; }

        /// <summary> 收貨人電話2 </summary>
        public string ReceiverTel2 { get; set; }

        /// <summary> 收貨人地址 </summary>
        public string ReceiverAddress { get; set; }

        /// <summary> 代收貨款 </summary>
        public string COD { get; set; }

        /// <summary> 發送日期 </summary>
        public string PickupDate { get; set; }
        /// <summary> 發送站代號 </summary>
        public string PickupStationId { get; set; }
        /// <summary> 到著站代號 </summary>
        public string ReceiverStationId { get; set; }

        /// <summary> 板數 </summary>
        public string TotalPallet { get; set; }
        /// <summary> 件數 </summary>
        public string TotalCarton { get; set; }

        /// <summary> 才數 </summary>
        public string TotalCbm { get; set; }
        /// <summary> 才數尺寸 </summary>
        public string CbmSize { get; set; }

        /// <summary> 指配日期 </summary>
        public string DeliveryDate { get; set; }

        /// <summary> 指配時間 </summary>
        public string DeliveryTimeZone { get; set; }

        /// <summary> 寄貨人代號 </summary>
        public string SenderId { get; set; }

        /// <summary> 寄貨人名稱 </summary>
        public string SenderName { get; set; }

        /// <summary> 寄貨人電話1 </summary>
        public string SenderTel1 { get; set; }

        /// <summary> 寄貨人電話2 </summary>
        public string SenderTel2 { get; set; }

        /// <summary> 寄貨人地址 </summary>
        public string SenderAddress { get; set; }

        /// <summary> 備註 </summary>
        public string Notes { get; set; }

        /// <summary> 回單 </summary>
        public string DeliveryReceiptQty { get; set; }

        /// <summary> 運送方式 0:全勤配送, 1:峻富配送, 2:宅配通配送, -9:無此貨運單 </summary>
        public string trans_type { get; set; }

        /// <summary> 配送狀態 已配達,到著站,未收件,無此貨運單 </summary>
        public string status { get; set; }
        /// <summary> 貨運單號 </summary>
        public string delivery_number { get; set; }

    }

    public class tcDeliveryRequests
    {
        /// <summary> 託運單號碼 </summary>
        public string checknumber { get; set; }

        /// <summary> 訂單編號 </summary>
        public string ordernumber { get; set; }

        /// <summary> 客戶代號 </summary>
        public string customercode { get; set; }

        /// <summary> 收貨人編號 </summary>
        public string receivecustomercode { get; set; }

        /// <summary> 傳票類別 </summary>
        public string subpoenacategory { get; set; }

        /// <summary> 代收貨款 </summary>
        public string collectionmoney { get; set; }

        /// <summary> 收貨人名稱 </summary>
        public string receivecontact { get; set; }

        /// <summary> 收貨人電話1 </summary>
        public string receivetel1 { get; set; }

        /// <summary> 收貨人電話2 </summary>
        public string receivetel2 { get; set; }

        /// <summary> 收貨人地址 </summary>
        public string ReceiverAddress { get; set; }

        /// <summary> 代收貨款 </summary>
        public string COD { get; set; }

        /// <summary> 發送日期 </summary>
        public string printdate { get; set; }

        /// <summary> 發送站 </summary>
        public string suppliercode { get; set; }

        /// <summary> 到著碼 </summary>
        public string area_arrive_code { get; set; }

        /// <summary> 件數 </summary>
        public string plates { get; set; }

        /// <summary>
        /// 件數(零擔)
        /// </summary>
        public string pieces { get; set; }

        /// <summary> 尺寸 </summary>
        public string CbmSize { get; set; }

        /// <summary> 指配日期 </summary>
        public string arriveassigndate { get; set; }

        /// <summary> 指配時間 </summary>
        public string timeperiod { get; set; }

        /// <summary> 寄貨人代號 </summary>
        public string SenderId { get; set; }

        /// <summary> 寄貨人名稱 </summary>
        public string sendcontact { get; set; }

        /// <summary> 寄貨人電話1 </summary>
        public string sendtel { get; set; }

        /// <summary> 寄貨人地址 </summary>
        public string SenderAddress { get; set; }

        /// <summary> 備註 </summary>
        public string invoicedesc { get; set; }

        /// <summary> 回單 </summary>
        public string receiptflag { get; set; }
        /// <summary> 來回件 </summary>
        public string roundtrip { get; set; }
        /// <summary> 產品編號 </summary>
        public string ArticleNumber { get; set; }
        /// <summary> 出貨平台 </summary>
        public string SendPlatform { get; set; }
        /// <summary> 品名 </summary>
        public string ArticleName { get; set; }

        public string ProductId { get; set; }
        public string SpecCodeId { get; set; }
        public string ShuttleStationCode { get; set; }
        public int SpecialAreaFee { get; set; }
        public int SpecialAreaId { get; set; }



    }

    public class trueJson
    {
        public bool isSuccess { get; set; }
        public int totalcountdt { get; set; }
        public object[] Request_Status_Detail { get; set; }
    }
    public class Request_Status_Detail
    {
        public string Type { get; set; }
        public string Time { get; set; }
        public string Salesrep_Code { get; set; }
        public string Salesrep_Phone { get; set; }
        public string Question_type { get; set; }
        public string Question_Memo { get; set; }

    }
    public class Request_Detail
    {
        public string check_number { get; set; }
        public string pricing_type { get; set; }
        public string order_number { get; set; }
        public string receiveaddress { get; set; }
        public string sendaddress { get; set; }
        public string Distributor { get; set; }
    }
    public class Request_MOMO_Detail
    {
        /// <summary> NO. </summary>
        public string NO { get; set; }

        /// <summary> 作業時間 </summary>
        public string scan_date { get; set; }

        /// <summary> 作業 </summary>
        public string scan_name { get; set; }

        /// <summary> 站所 </summary>
        public string supplier_name { get; set; }

        /// <summary> 狀態 </summary>
        public string status { get; set; }

        /// <summary> 配送代碼 </summary>
        public string ItemCodes { get; set; }

        /// <summary> 員工代號 </summary>
        public string driver_code { get; set; }

        /// <summary> 員工姓名 </summary>
        public string driver_name { get; set; }

        /// <summary> 站所名稱 </summary>
        public string station_name { get; set; }
        /// <summary> 站所代碼 </summary>
        public string station_code { get; set; }
    }


    public class Request_errorMsg
    {
        /// <summary> NO. </summary>
        public bool isSuccess { get; set; }

        /// <summary> 作業時間 </summary>
        public string error_msg { get; set; }
    }

    public class Request_Msg
    {
        /// <summary> NO. </summary>
        public bool isSuccess { get; set; }
        public string msg { get; set; }
    }

    public class ADDRESS
    {
        public ADDRESS(string address)
        {
            this.OrginalAddress = address;
            this.ParseByRegex(address);
        }

        public static string GetDTName = "SELECT";

        ///縣市 
        public string City { get; set; }
        /// 鄉鎮市區
        public string Region { get; set; }
        /// 村里
        public string Village { get; set; }
        /// 鄰
        public string Neighbor { get; set; }
        /// 路
        public string Road { get; set; }
        /// 段
        public string Section { get; set; }
        /// 巷
        public string Lane { get; set; }
        /// 弄
        public string Alley { get; set; }
        /// 號
        public string No { get; set; }
        /// 號
        public string Seq { get; set; }
        /// 樓
        public string Floor { get; set; }
        public string Others { get; set; }
        /// 是否符合pattern規範
        public bool IsParseSuccessed { get; set; }
        /// 原始傳入的地址
        public string OrginalAddress { get; private set; }
        private void ParseByRegex(string address)
        {
            String pattern = "(?<zipcode>(^\\d{5}|^\\d{3})?)(?<city>\\D+?[縣市])(?<region>\\D+?(市區|鎮區|鎮市|[鄉鎮市區]))?(?<village>\\D+?[村里])?(?<neighbor>\\d+[鄰])?(?<road>\\D+?(村路|[路街道段]))?(?<section>\\D?段)?(?<lane>\\d+巷)?(?<alley>\\d+弄)?(?<no>\\d+號?)?(?<seq>-\\d+?(號))?(?<floor>\\d+樓)?(?<others>.+)?";


            Match match = Regex.Match(address, pattern);

            if (match.Success)
            {
                this.IsParseSuccessed = true;
                this.City = match.Groups["city"].ToString();
                this.Region = match.Groups["region"].ToString();
            }

        }
    }

    public class Shopee
    {
        /// <summary>
        /// 託運單號
        /// </summary>
        public string checknumber { get; set; }
    }



    public class AddressParsingInfo
    {
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 鄉鎮區
        /// </summary>
        public string Town { get; set; }
        /// <summary>
        /// 路段
        /// </summary>
        public string Road { get; set; }
        /// <summary>
        /// 郵局三碼
        /// </summary>
        public string PostZip3 { get; set; }
        /// <summary>
        /// 郵局3+2
        /// </summary>
        public string PostZip32 { get; set; }
        /// <summary>
        /// 郵局3+3
        /// </summary>
        public string PostZip33 { get; set; }
        /// <summary>
        /// 站所代碼
        /// </summary>
        public string StationCode { get; set; }
        /// <summary>
        /// 站所名稱
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// 配送貨車司機
        /// </summary>
        public string SalesDriverCode { get; set; }
        /// <summary>
        /// 配送機車司機
        /// </summary>
        public string MotorcycleDriverCode { get; set; }
        /// <summary>
        ///  堆疊區
        /// </summary>
        public string StackCode { get; set; }
        /// <summary>
        ///  接泊碼
        /// </summary>
        public string ShuttleStationCode { get; set; }
        public string sendSalesDriverCode { get; set; }
        public string sendMotorcycleDriverCode { get; set; }


        //public AddressParsingInfo() 
        //{
        //    ShuttleStationCode = string.Empty;


        //}
    }

    public class SpecialAreaManage
    {
        public long id { get; set; }
        public string City { get; set; }
        public string Town { get; set; }
        public string Road { get; set; }
        public int Lane { get; set; }
        public int Alley { get; set; }
        public int Even { get; set; }
        public int NoStart { get; set; }
        public int NoEnd { get; set; }
        public string KeyWord { get; set; }
        public string StationCode { get; set; }
        public string ShuttleStationCode { get; set; }
        public string SalesDriverCode { get; set; }
        public string MotorcycleDriverCode { get; set; }
        public string StackCode { get; set; }
        public int? Fee { get; set; }
        public int? TimeLimit { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreateUser { get; set; }
        public string UpdateUser { get; set; }

    }






}
